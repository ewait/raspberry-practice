#include <stdio.h>

int Add(int a,int b)
{
    printf("Add(a,b)\n");
    return a+b;
}
//test

#include <unistd.h>
int main()
{
    int i=2000;
	while (i>=0)
	{
		printf("%d\r",i);
		//在linux环境中，不带'\n'的时候，并不会打印（没有刷新缓存区）
		//而在VS环境中，带不带都会正常打印
        fflush(stdout);//手动刷新缓冲区
		sleep(1);//linux环境中，sleep函数的参数，单位是秒（VS是毫秒）
        //             linux环境下，sleep函数需要小写，VS下是Sleep
        i--;
	}
	return 0;
}