#include<stdio.h>
#include<unistd.h>
#include<string.h>
#include<stdlib.h>

//一个简单的计算器
void Calculator(char* argv[])
{
    int x = atoi(argv[2]);
    int y = atoi(argv[3]);
    if(strcmp("-a", argv[1]) ==0)
    {
        printf("%d+%d=%d\n",x, y, x + y);
    }
    else if(strcmp("-s", argv[1]) ==0)
    {
        printf("%d-%d=%d\n",x, y, x - y);
    }
    else if(strcmp("-m", argv[1]) ==0)
    {
        printf("%d*%d=%d\n",x, y, x * y);
    }
    else if(strcmp("-d", argv[1]) ==0 && y != 0)
    {
        printf("%d/%d=%d\n",x, y, x / y);
    }
    else
    {
        printf("Usage: %s [-a|-s|-m|-d] first second\n", argv[0]);
    }
    exit(0);
}

//main函数可以带参数
//第一个参数指代命令个数，执行该可执行文件时传入的几个命令
//第二个参数是一个指针数组，存放了每一个命令的常量字符串
//第三个参数用于导入环境变量！
int main(int arg,char* argv[],char *envs[])
{
    // printf("arg: %d\n",arg);
    // for(int i =0;i<arg;i++)
    // {
    //     printf("argv[%d]: %s\n",i,argv[i]);
    // }

    // for(int i =0;envs[i];i++)
    // {
    //     printf("envs[%d]: %s\n",i,envs[i]);
    // }

    extern char ** environ;
    printf("get env from [environ]\n");
    for(int i = 0; environ[i]; i++)
    {
        printf("%d: %s\n", i, environ[i]);
    }

    // 命令行计算器
    // if(arg != 4)
    // {
    //     printf("Usage: %s [-a|-s|-m|-d] first second\n", argv[0]);
    //     return 0;
    // }
    // else
    // {
    //     Calculator(argv);
    // }


    return 0;
}