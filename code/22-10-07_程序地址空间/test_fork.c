#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>

int main()
{
    int test = 10;
    int ret = fork();
    if(ret == 0)
    {
        //int i =0;
        while(1)
        {
            //printf("我是子进程%d,ppid:%d,test:%d,&test: %p\n\n",getpid(),getppid(),test,&test);
            printf("我是子进程%d,ppid:%d,ret:%d,&ret:%p\n\n",getpid(),getppid(),ret,&ret);
            //i++;
            sleep(1);
            // if(i==4){
            //     printf("子进程修改test=20\n");
            //     test=20;
            //     i++;
            // }
        }
    }
    else
    {    
        while(1)
        {
            //printf("我是父进程%d,ppid:%d,test:%d,&test: %p\n\n",getpid(),getppid(),test,&test);
            printf("我是父进程%d,ppid:%d,ret:%d,&ret:%p\n\n",getpid(),getppid(),ret,&ret);
            sleep(1);
        }
    }       
    return 0;
}
