#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

void testwait()
{
    int test = 10;
    pid_t ret = fork();
    if(ret == 0)
    {
        int i = 5;
        while(i--)
        {
            printf("我是子进程%-5d, ppid:%-5d, ret:%-5d, &ret:%p\n",getpid(),getppid(),ret,&ret);
            //i++;
            sleep(1);
        }
        printf("子进程退出\n");
        exit(200);
    }
    else
    {    
        printf("我是父进程%-5d, ppid:%-5d, ret:%-5d, &ret:%p\n\n",getpid(),getppid(),ret,&ret);
        sleep(5);
        int status = 0;
        pid_t st = wait(&status);
        //printf("等待成功,子进程pid:%d, 状态信息：%d\n",st,status);//直接打印status是错误的！
        //status的低16位才有效，其中这16位的高8位是状态码
        printf("等待成功,子进程pid:%d, 状态：%d，信号：%d\n",st,(status>>8)&0xFF,status&0x7F);//0xff是8个1
        sleep(5);
    }      
}

int main()
{
    int test = 10;
    pid_t ret = fork();
    if(ret == 0)
    {
        int i = 10;
        while(i--)
        {
            printf("我是子进程%-5d, ppid:%-5d, ret:%-5d, &ret:%p\n",getpid(),getppid(),ret,&ret);
            //i++;
            sleep(1);
        }
        printf("子进程退出\n");
        exit(11);
    }
    else
    {    
        printf("我是父进程%-5d, ppid:%-5d, ret:%-5d, &ret:%p\n\n",getpid(),getppid(),ret,&ret);
        sleep(11);
        int status = 0;
        pid_t st = waitpid(ret,&status,0);//指定等待上面创建的子进程
        if(WIFEXITED(status))//子进程正常退出返回真
        {   
            printf("等待成功,子进程pid:%d, 状态：%d，信号：%d\n",st,WEXITSTATUS(status),WTERMSIG(status));
        }
        else
        {
            printf("非正常退出,子进程pid:%d, 状态：%d，信号：%d\n",st,WEXITSTATUS(status),WTERMSIG(status));
        }
        
        sleep(10);
    }      

    return 0;
}
