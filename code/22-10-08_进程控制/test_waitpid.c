#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

int add(int a,int b){
    return a+b;
}
int pls(int a,int b){
    return a*b;
}

int main()
{
    pid_t id = fork();
    if(id == 0)
    {
        // 子进程
        int i =3;
        while(i--)
        {
            printf("我是子进程, 我的PID: %d, 我的PPID:%d\n", getpid(), getppid());
            sleep(2);
        }
        exit(0);
    }
    else if(id >0)
    {
        // 父进程
        // 基于非阻塞的轮询等待方案
        int status = 0;
        int i = 1, j=2;
        while(1)
        {
            pid_t ret = waitpid(-1, &status, WNOHANG);
            if(ret > 0)
            {
                printf("等待成功, %d, exit code: %d, exit sig: %d\n", ret, WIFEXITED(status), WTERMSIG(status));
                break;
            }
            else if(ret == 0)
            {
                //等待成功了，但子进程没有退出
                printf("子进程好了没？没有，父进程做其他事情\n");
                printf("add %d  ",add(i++,j++));
                printf("pls %d\n",pls(i++,j++));
                sleep(1);
            }
            else{
                //err
                printf("父进程等待出错！\n");
                break;
            }
        }
    }
    return 0;
}