#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>


void testExecl()
{
    printf("开始测试\n\n");
    int ret = execl("/usr/local/bin/python3","python3","test.py",NULL);
    //int ret = execl("/usr/bin/ls","ls","-l",NULL);
    printf("执行结束: %d\n",ret);
    printf("错误原因: %s\n",strerror(errno));
    return ;
}

void testExecv()
{
    printf("开始测试\n\n");
    // char*const arg[]={
    //     (char*)"ls",
    //     (char*)"-l",
    //     (char*)"-a",
    //     NULL
    // };
    char*const arg[]={
        "ls",
        "-l",
        "-a",
        NULL
    };
    int ret = execv("/usr/bin/ls",arg);
    //int ret = execl("/usr/bin/ls","ls","-l",NULL);
    printf("执行结束: %d\n",ret);
    printf("错误原因: %s\n",strerror(errno));
    return ;
}


void testExeclp()
{
    printf("开始测试\n\n");
    int ret = execlp("python3","python3","test.py",NULL);
    printf("执行结束: %d\n",ret);
    printf("错误原因: %s\n",strerror(errno));
    return ;
}

void testExecvp()
{
    printf("开始测试\n\n");
    char*const arg[]={
        "ls",
        "-l",
        "-a",
        NULL
    };
    int ret = execvp("ls",arg);
    printf("执行结束: %d\n",ret);
    printf("错误原因: %s\n",strerror(errno));
    return ;
}


void testExecve()
{
    extern char ** environ;//引入系统环境变量
    printf("开始测试\n\n");
    char*const arg[]={
        "./mytest",
        NULL
    };
    char*const arg_[]={
        "ls",
        "-l",
        "-a",
        NULL
    };
    char*const env[]={
        "PATH=path test",
        "MYPATH=this is c test",
        NULL
    };//覆盖系统环境变量
    //int ret = execle("/home/muxue/git/linux/code/22-10-13_exec/mytest","./mytest",NULL,env);
    //int ret = execve("/home/muxue/git/linux/code/22-10-13_exec/mytest",arg,env);
    int ret = execvpe("ls",arg_,env);
    printf("执行结束: %d\n",ret);
    printf("错误原因: %s\n",strerror(errno));
    return ;
}


int add(int a,int b){
    return a+b;
}
int pls(int a,int b){
    return a*b;
}

void testWiatPid()
{
    pid_t id = fork();
    if(id == 0)
    {
        // 子进程
        int i = 3;
        while(i--)
        {
            printf("我是子进程, 我的PID: %d, 我的PPID:%d\n", getpid(), getppid());
            sleep(2);//便于观察
            int ret = execl("/usr/local/bin/python3","python3","test.py",NULL);
            printf("子进程执行出错: %d\n",ret);
            printf("子进程错误原因: %s\n",strerror(errno));
            exit(-1);
        }
    }
    else if(id >0)
    {
        // 父进程
        // 基于非阻塞的轮询等待方案
        int status = 0;
        int i = 1, j=2;
        printf("我是父进程, 我的PID: %d, 我的PPID:%d\n", getpid(), getppid());
        while(1)
        {
            pid_t ret = waitpid(-1, &status, WNOHANG);
            if(ret > 0)
            {
                printf("等待成功, %d, exit code: %d, exit sig: %d\n", ret, WIFEXITED(status), WTERMSIG(status));
                break;
            }
            else if(ret == 0)
            {
                //等待成功了，但子进程没有退出
                printf("子进程好了没？没有，父进程做其他事情\n");
                printf("add %d  ",add(i++,j++));
                printf("pls %d\n",pls(i++,j++));
                sleep(1);
            }
            else{
                //err
                printf("父进程等待出错！\n");
                break;
            }
        }
    }
    return ;
}

int main()
{
    testExecve();
    return 0;
}