#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/wait.h>
#include <sys/types.h>

#define SEP " " //分隔符
#define NUM 1024
#define SIZE 128

char cmd_line[NUM];       //命令行输入
char *cmd_args[SIZE];     //分离参数
char cmd_line_alias[NUM]; //给别名用的临时数组
size_t cmd_args_num = 0;  //分离出来的参数个数

//用一个buffer来存放环境变量
char env_buffer[SIZE][NUM];
size_t env_num = 0; //环境变量的数量
//系统的环境变量
extern char **environ;
//变量名别名
typedef struct alias_cmd
{
    char _cmd[SIZE];
    char _acmd[SIZE];
} alias;
alias cmd_alias[SIZE]; //缓存别名键值对(结构体)
size_t alias_num = 0;  //已缓存的别名个数
//设置别名(新命令，原命令)
void set_alias(char *cmd, char *acmd)
{
    //查找别名里是否已经有了这个
    for (int i = 0; i < alias_num; i++)
    {
        if (strcmp(cmd_alias[i]._cmd, cmd) == 0) //是已有的别名
        {
            strcpy(cmd_alias[i]._acmd, acmd);
            // printf("set cmd %s acmd %s\n",cmd_alias[i]._cmd,cmd_alias[i]._acmd);
            return;
        }
    }

    //没有提前退出，说明是新增别名
    strcpy(cmd_alias[alias_num]._cmd, cmd);
    strcpy(cmd_alias[alias_num]._acmd, acmd);
    alias_num++;
}
//判断一个命令是否有别名
bool is_alias(char *cmd_args[], int sz)
{
    int i = 0;
    for (i = 0; i < alias_num; i++)
    {
        if (strcmp(cmd_alias[i]._cmd, cmd_args[0]) == 0) //是别名
        {
            size_t index = 1, j;
            char *cmd_args_temp[SIZE];                                           //临时数组用于分离别名里面的命令
            memset(cmd_line_alias, '\0', sizeof(cmd_line_alias) * sizeof(char)); //清空别名命令缓存
            //先把别名中的命令分开
            strcpy(cmd_line_alias, cmd_alias[i]._acmd); //不能直接使用_acmd，不然会影响下次别名使用
            cmd_args_temp[0] = strtok(cmd_line_alias, SEP);
            //别名的时候也需要设置ls的颜色（需要保证原本命令的第一个不是ls，不然本来就已经有"--color=auto"了）
            if (strcmp(cmd_args_temp[0], "ls") == 0 && strcmp(cmd_args[0], "ls") != 0)
                cmd_args_temp[index++] = (char *)"--color=auto";
            while (cmd_args_temp[index++] = strtok(NULL, SEP))
                ;
            index--; // while会多+1，需要重新操作一下
            //从原本数组的第二位开始往后设置
            for (j = 1; j < cmd_args_num; j++)
            {
                cmd_args_temp[index++] = cmd_args[j];
            }
            //替换掉原本的数组
            cmd_args_num = index;              //此时的index长度正确，不需要-1
            for (j = 0; j < cmd_args_num; j++) //因为while最后会多++一次，所以需要-1
            {
                //原本的位置没有那么大空间，放不下，不能拷贝
                // strcpy(cmd_args[j],cmd_args_temp[j]);
                cmd_args[j] = cmd_args_temp[j];
                // printf("temp[%d] %s   args[%d] %s\n",j,cmd_args_temp[j],j,cmd_args[j]);
            }
            cmd_args[j] = NULL; //最后一个位置设置成NULL
            return true;
        }
    }
    return false;
}

// 内建命令

//这里导入环境变量之后，不会影响linux的shell
//而是从我们的myshell开始所有子进程都会继承
int PutEnvIn(char *new_env)
{
    putenv(new_env);
    return 0;
}

//对应上层的内建命令
//不使用内建命令，则不会生效
int ChangeDir(const char *new_path)
{
    chdir(new_path);
    return 0; // 调用成功
}

void AliasCmdSet(char *alias[])
{
    char alias_str[SIZE];
    // printf("AMS %d, ALIAS[1] %s\n",cmd_args_num,alias[1]);
    strcpy(alias_str, alias[1]);
    int i = 2; //往后追加
    while (i < cmd_args_num)
    {
        strcat(alias_str, " ");
        strcat(alias_str, alias[i++]);
    }
    // printf("ams %s\n",alias_str);

    char *p = strstr(alias_str, "=");
    if (p == NULL)
    {
        printf("alias set err! no '=' in cmd\n");
        return;
    }
    *p = '\0'; //把=号的位置设置为\0
    // 往后查找是否有''
    char *p1 = strstr(p + 1, "'");
    if (p1 != NULL)
    {
        char *p2 = strstr(p1 + 1, "'");
        if (p2 == NULL)
        {
            printf("alias set err! no closing quotation\n");
            return;
        }
        else
        {
            *p2 = '\0'; //把结尾的'设置为\0
            set_alias(alias_str, p1 + 1);
        }
    }
    else //没有'代表只有一条命令
    {
        set_alias(alias_str, p + 1);
    }
    // printf("ACS cmd: %s acmd: %s\n",cmd_alias[alias_num-1]._cmd,cmd_alias[alias_num-1]._acmd);
    cmd_args_num++;
}

// 主函数
int main()
{

    // shell一直死循环
    while (1)
    {
        // 1.显示命令行中的提示符
        char cur_pwd[SIZE] = "~";
        int sz_pwd = strlen(getenv("HOME"));
        strcat(cur_pwd, getenv("PWD") + sz_pwd);
        printf("[慕雪@FS-1041 %s]# ", cur_pwd);
        //printf("[慕雪@FS-1041 当前路径]# ");
        fflush(stdout); //刷新缓冲区，达到不换行的效果

        // 2.获取用户的输入内容
        memset(cmd_line, '\0', sizeof(cmd_line) * sizeof(char));
        fgets(cmd_line, NUM, stdin);           //标准输入stdin获取键盘输入内容
        if(strlen(cmd_line)==1)
        {
            continue;//等于1的情况只能是敲了一个回车
        }
        // 其他情况去掉末尾的\n回车
        cmd_line[strlen(cmd_line) - 1] = '\0'; 
        // printf("%d\n",cmd_line);//打印内容作为测试

        // 3.分离出命令和参数
        cmd_args[0] = strtok(cmd_line, SEP);
        cmd_args_num = 1;
        // 给ls命令添加颜色
        if (strcmp(cmd_args[0], "ls") == 0)
            cmd_args[cmd_args_num++] = (char *)"--color=auto";
        // strtok 截取成功，返回字符串起始地址
        // strtok 第二次传入null会从上一次数组截取到的位置继续往后找
        // 截取失败，=赋值操作符返回左值，为NULL
        while (cmd_args[cmd_args_num++] = strtok(NULL, SEP));
        cmd_args_num--;//这里-1是因为while循环最后会多++一次
        // for(int j=0;j<cmd_args_num;j++){
        //     printf("args[%d] %s\n",j,cmd_args[j]);
        // }
        // printf("\n");

        // 4.支持别名
        // set_alias("ll","ls -l");// ls的别名
        is_alias(cmd_args, cmd_args_num); //判断并替换别名

        // 5.内建命令
        if (strcmp(cmd_args[0], "cd") == 0 && cmd_args[1] != NULL)
        {
            ChangeDir(cmd_args[1]); //让调用方进行路径切换, 父进程
            continue;
        }
        if (strcmp(cmd_args[0], "export") == 0 && cmd_args[1] != NULL)
        {
            // 目前，环境变量信息在cmd_line,会被清空
            // 此处我们需要自己保存一下环境变量内容
            strcpy(env_buffer[env_num], cmd_args[1]);
            PutEnvIn(env_buffer[env_num]);
            env_num++;
            continue;
        }
        if (strcmp(cmd_args[0], "alias") == 0 && cmd_args[1] != NULL)
        {
            AliasCmdSet(cmd_args); //起别名
            continue;
        }

        // 6.创建程序 替换
        pid_t ret_id = fork();
        if (ret_id == 0) //子进程
        {
            execvp(cmd_args[0], cmd_args); //程序替换
            exit(134);                     //执行到这里，子进程一定替换失败
        }
        // 父进程
        int status = 0;
        pid_t ret = waitpid(ret_id, &status, 0);
        printf("\n");
        if (ret > 0)
        {
            printf("bash等待子进程成功！code: %d, sig: %d\n", WEXITSTATUS(status), WTERMSIG(status));
            if (WEXITSTATUS(status) == 134) //子进程替换程序失败
                printf("execvp err: %s\n", strerror(errno));
        }
        else
        {
            printf("bash等待子进程失败！code: %d, sig: %d\n", WEXITSTATUS(status), WTERMSIG(status));
        }
    }
    return 0;
}