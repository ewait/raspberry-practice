#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <assert.h>

#define NUM 1024

#define NONE_FLUSH 0x0 //无刷
#define LINE_FLUSH 0x1 //行刷
#define FULL_FLUSH 0x2 //全缓存

typedef struct MyFILE{
    int _fileno;
    char _buffer[NUM];
    int _end;
    int _fflags; //fflush 刷新策略
}MyFILE;

MyFILE *my_fopen(const char *filename, const char *method)
{
    assert(filename);
    assert(method);

    int flags = O_RDONLY;

    if(strcmp(method, "r") == 0)
    {
        flags = O_RDONLY;
    }
    else if(strcmp(method, "w") == 0)
    {
        flags = O_WRONLY | O_CREAT | O_TRUNC;
    }
    else if(strcmp(method, "a") == 0)
    {
        flags = O_WRONLY | O_CREAT | O_APPEND;
    }

    int fileno = open(filename, flags, 0666);
    if(fileno < 0)
    {
        return NULL;
    }

    MyFILE *fp = (MyFILE *)malloc(sizeof(MyFILE));
    if(fp == NULL) return fp;
    memset(fp, 0, sizeof(MyFILE));
    fp->_fileno = fileno;
    fp->_fflags |= LINE_FLUSH;
    fp->_end = 0;
    return fp;
}

void my_fflush(MyFILE *fp)
{
    assert(fp);

    if(fp->_end > 0)
    {
        write(fp->_fileno, fp->_buffer, fp->_end);
        fp->_end = 0;
        syncfs(fp->_fileno);
    }
}


void my_fwrite(MyFILE *fp, const char *start, int len)
{
    assert(fp);
    assert(start);
    assert(len > 0);

    // abcde123
    // 写入到缓冲区里面
    strncpy(fp->_buffer+fp->_end, start, len); //将数据写入到缓冲区了
    fp->_end += len;

    if(fp->_fflags & NONE_FLUSH)
    {}
    else if(fp->_fflags & LINE_FLUSH)
    {
         //这里的判断只是测试，实际上还需要判断中间有无\n
        if(fp->_end > 0 && fp->_buffer[fp->_end-1] == '\n')
        {
            //写入到内核中
            write(fp->_fileno, fp->_buffer, fp->_end);
            fp->_end = 0;
            syncfs(fp->_fileno);
        }
    }
    else if(fp->_fflags & FULL_FLUSH)
    {
        //满了刷新
        if(fp->_end > 0 && fp->_end==NUM)
        {
            //写入到内核中
            write(fp->_fileno, fp->_buffer, fp->_end);
            fp->_end = 0;
            syncfs(fp->_fileno);
        }
    }
}

void my_fclose(MyFILE *fp)
{
    my_fflush(fp);
    close(fp->_fileno);
    free(fp);
}


void test1(MyFILE * fp)
{
    const char *a = "hello my 111\n";
    my_fwrite(fp, a, strlen(a));

    printf("消息立即刷新\n");
    sleep(3);

    const char *b = "hello my 222 ";
    my_fwrite(fp, b, strlen(b));
    printf("写入了一个不满足刷新条件的字符串\n");
    sleep(3);

    const char *c = "hello my 333 ";
    my_fwrite(fp, c, strlen(c));
    printf("写入了一个不满足刷新条件的字符串\n");
    sleep(3);


    const char *d = "end\n";
    my_fwrite(fp, d, strlen(d));
    printf("写入了一个满足刷新条件的字符串\n");
    sleep(3);
}

void test2(MyFILE * fp)
{
    const char *s = "-test ";
    my_fwrite(fp, s, strlen(s));
    printf("写入了一个不满足刷新条件的字符串\n");
    fork();
}


int main()
{
    MyFILE *fp = my_fopen("log.txt", "w");
    if(fp == NULL)
    {
        printf("my_fopen error\n");
        return 1;
    }

    //test1(fp);
    test2(fp);

    //模拟进程退出
    my_fclose(fp);
    printf("程序结束\n");
}
