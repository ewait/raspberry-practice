#include <stdio.h>
#include <iostream>
using namespace std;

int main()
{
    // stdout
    printf("hello printf\n");
    fprintf(stdout, "hello fprintf to stdout\n");
    fputs("hello fputs to stdout\n", stdout);
    cout << "hello cout" << endl;

    // stderr
    perror("hello perror");
    fprintf(stderr, "hello fprintf to stderr\n");
    fputs("hello fputs to stderr\n", stderr);
    cerr << "hello cerr" << endl;

    return 0;
}