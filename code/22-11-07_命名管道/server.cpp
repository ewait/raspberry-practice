#include"MyPath.h"

int main()
{
    //创建管道文件
    umask(0);
    if(mkfifo(FILE_PATH, 0600) != 0)
    {
        cerr << "mkfifo error" << endl;
        return 1;
    }
    //打开管道文件O_WRONLY
    //int pipeFd = open(FILE_PATH, O_WRONLY);//for test
    int pipeFd = open(FILE_PATH, O_RDONLY);
    if(pipeFd < 0)
    {
        cerr << "open fifo error" << endl;
        return 2;
    }

    //开始通信
    cout << "服务器启动" << endl;
    char buffer[NUM];
    while(1)
    {
        //服务端执行读
        ssize_t s = read(pipeFd, buffer, sizeof(buffer)-1);
        if(s > 0)
        {
            buffer[s] = '\0';
            cout << "客户端->服务器# " << buffer << endl;
        }
        else if(s == 0)
        {
            cout << "客户端退出，服务器终止接收" << endl;
            break;
        }
        else
        {
            cout << "read: " << strerror(errno) << endl;
            break;
        }
    }


    close(pipeFd);
    cout << "服务器关闭" << endl;
    unlink(FILE_PATH);
    return 0;
}