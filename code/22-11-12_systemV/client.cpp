#include "Mykey.hpp"

int main()
{
    //获取key值
    key_t key = CreateKey();
    //获取共享内存
    int id = shmget(key, NUM, IPC_CREAT);
    if(id<0)
    {
        cerr<< "shmget err: " << strerror(errno) << endl; 
        return 1;
    }
    cout << "shmget success: " << id << endl;

    //获取管道
    int fd = Open(O_WRONLY);
    cout << "open fifo success: " << fd << endl;
    sleep(2);
    //关联共享内存
    char *str = (char*)shmat(id, nullptr, 0);
    printf("[client] shmat success\n");
    //写入数据
    int i=0;
    while(i<26)
    {
        char base = 'A';
        str[i] = base+i;
        str[i+1] = '\0';
        printf("write times: %02d\n",i);
        i++;
        Signal(fd);
        sleep(1);
    }


    //去关联
    shmdt(str);//shmat的返回值
    printf("[client] shmdt & exit\n");
    close(fd);
    printf("[client] close fifo\n");
    return 0;
}