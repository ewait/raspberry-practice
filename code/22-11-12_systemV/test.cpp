#include "Mykey.hpp"

int main()
{
    //获取key值
    key_t key = CreateKey();
    //创建共享内存
    int id = shmget(key, NUM, IPC_CREAT | IPC_EXCL | 0666);
    if(id<0)
    {
        cerr<< "shmget err: " << strerror(errno) << endl; 
        return 1;
    }
    cout << "shmget success: " << id << endl;

    sleep(5);
    //关联共享内存
    char *str = (char*)shmat(id, nullptr, 0);

    //写入
    int i=0;
    while(1)
    {
        char base = 'A';
        str[i++] = base+i;
        str[i] = '\0';
    }

    //去关联
    shmdt(str);//shmat的返回值
    //删除共享内存
    shmctl(id,IPC_RMID,nullptr);
    return 0;
}