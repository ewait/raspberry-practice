#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/unistd.h>


int main()
{
    int semid;
    key_t key;
    struct sembuf semaphore;

    // 创建或获取信号量集
    key = ftok(".", 'S');
    semid = semget(key, 1, IPC_CREAT | 0666);
    if (semid == -1)
    {
        perror("Failed to create semaphore");
        exit(1);
    }

    // 初始化信号量的值为1
    if (semctl(semid, 0, SETVAL, 1) == -1)
    {
        perror("Failed to initialize semaphore value");
        exit(1);
    }

    // 对信号量进行操作
    semaphore.sem_num = 0;
    semaphore.sem_op = -1; // 减少信号量值
    semaphore.sem_flg = SEM_UNDO;
    if (semop(semid, &semaphore, 1) == -1)
    {
        perror("Failed to perform semaphore operation");
        exit(1);
    }

    // 执行需要同步的代码
    printf("begin sleep 5\n");
    sleep(5);
    printf("end sleep 5\n");
    sleep(1);

    // 释放信号量
    semaphore.sem_op = 1; // 增加信号量值
    if (semop(semid, &semaphore, 1) == -1)
    {
        perror("Failed to release semaphore");
        exit(1);
    }

    // 删除信号量集
    if (semctl(semid, 0, IPC_RMID) == -1)
    {
        perror("Failed to remove semaphore");
        exit(1);
    }

    return 0;
}
