#include <iostream>
#include <cstdlib>
#include <cstring>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
using namespace std;
//实现一个自己的kill命令
void mykill(int argc,char *argv[])
{
    if(argc != 3)
    {
        cout << "Usage: " << argv[0] << " signo-id process-id" <<endl;
        exit(1);
    }

    if(kill(static_cast<pid_t>(atoi(argv[2])), atoi(argv[1])) == -1)
    {
        cerr << "kill: " << strerror(errno) << endl;
        exit(2);//出现错误
    }
    exit(0);//正常执行
}

int main(int argc, char *argv[])
{
    mykill(argc,argv);

    return 0;
}