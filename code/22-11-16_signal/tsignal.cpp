#include <iostream>
#include <cstdlib>
#include <cstring>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
using namespace std;

//打印收到的信号
void handler(int signo)
{
    cout << "process " << getpid() << " get signal: " << signo << endl;
}

void TestSignal()
{
    //对所有的进程信号都设置一个回调
    for (int sig = 1; sig <= 31; sig++)
    {
        signal(sig, handler);
    }

    // signal(2, handler);//将二号信号设置一个回调，其余信号不做处理
    cout << "进程信号已经设置完了" << endl;
    sleep(3);
}

void test1()
{
    int a = 10;
    int b = 0;
    try
    {
        int c = a / b; // C++的除0不是异常，不会抛出
        //所以会直接linux系统运行报错
    }
    catch (const exception &e)
    {
        cerr << "a/0 err" << endl;
        abort();
    }
    catch (...)
    {
        cout << "base catch" << endl;
        abort();
    }
}


void TestAlarm()
{
    // TestSignal();//设置对进程信号的屏蔽
    alarm(4); // 4s后向自己发送信号
    cout << "set alarm, sleep" << endl;
    sleep(8);
    cout << "sleep finish" << endl;
}

void test2()
{
    while(1)
    {
        //raise(2);
        abort();//向自己发送6号信号
        sleep(1);
    }
}

void testfork1()
{
    int status;
    int id = fork();
    if(id == 0)
    {
        //子进程
        cout << "chlid process: " <<getpid()<<endl;
        int b=0;
        int a = 10/b;
    }
    TestSignal();
    int ret = waitpid(id,&status,0);
    //打印子进程的退出信息
    printf("exitcode:%d signo:%d coredump: %d\n",(status>>8)&&0xff,status&0x7f,(status>>7)&0x1);
}
void testfork2()
{
    int status;
    int id = fork();
    if(id == 0)
    {
        //子进程
        while(1)
        {
            cout << "chlid process: " <<getpid()<<endl;
            sleep(1);
        }   
    }
    //父进程自定义捕捉信号
    TestSignal();
    while(1)
    {
        cout << "dad   process: " <<getpid()<<endl;
        sleep(1);
    }
    int ret = waitpid(id,&status,0);
    //打印子进程的退出信息
    printf("exitcode:%d signo:%d coredump: %d\n",(status>>8)&&0xff,status&0x7f,(status>>7)&0x1);
}



//打印信号集的内容
void showPending(sigset_t* pdg_ptr)
{
    for(int i=1;i<32;i++)
    {
        if(sigismember(pdg_ptr,i))
        {
            cout << "1";
        }
        else
        {
            cout << "0";
        }
    }
    cout << endl;
}
//测试block相关接口
void TestSigset()
{
    //block掉所有信号
    sigset_t nsig,osig;
    sigemptyset(&nsig);
    sigemptyset(&osig);
    for(int i=1;i<32;i++)
    {
        sigaddset(&nsig,i);//在nsig中设置2为1
    }
    sigprocmask(SIG_BLOCK,&nsig,&osig);//添加屏蔽

    TestSignal();//设置信号自定义处理
    cout << "start process: " << getpid() << endl;
    sigset_t pdg;
    int k=15;
    while(k--)
    {
        sigemptyset(&pdg);//初始化信号集
        if(sigpending(&pdg)==0)//获取
        {
            showPending(&pdg);//获取成功，打印
        }
        else
        {
            cout << getpid() << " get pending err"<<endl;
        }
        sleep(1);
    }

    //利用osig恢复之前的block表
    sigprocmask(SIG_SETMASK,&osig,nullptr);
    sigemptyset(&pdg);//初始化信号集
    if(sigpending(&pdg)==0)//获取
    {
        showPending(&pdg);//获取成功，打印
    }
    sleep(10);
    cout << "process quit"<<endl;
}

void TestSigaction()
{
    struct sigaction nact,oact;
    nact.sa_flags = 0;
    nact.sa_handler = handler;
    sigemptyset(&nact.sa_mask);//初始化
    //基本用法，自定义2号信号
    sigaction(2,&nact,&oact);

    while(1)
    {
        cout << "process running: " << getpid() << endl;
        sleep(2);
    }

}

int main(int argc, char *argv[])
{
    testfork2();
    return 0;
}