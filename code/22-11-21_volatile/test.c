#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>


// 保持内存的可见性
int flag = 0;

void handler(int signo)
{
    flag = 1;
    printf("\n更改flag: 0->1\n");
}

int main()
{   
    printf("process start %d\n",getpid());
    signal(2, handler);//自定义捕捉2号信号

    while (!flag)
        ;//啥事不干的循环
    
    printf("process exit!\n");

    return 0;
}
