#include<iostream>
#include<string.h>
#include<signal.h>
#include<pthread.h>
#include<thread>
#include<unistd.h>
#include<sys/types.h>
#include<sys/syscall.h>
using namespace std;

int water = 0;//全局变量
int cup = 10000;//杯子的容量
pthread_mutex_t mutex;

void* func(void* arg)
{
    while(1)
    {
        pthread_mutex_lock(&mutex);
        if(water<cup)
        {
            usleep(100);
            cout << (char*)arg << " 水没有满：" << water << "\n";
            water++;
            pthread_mutex_unlock(&mutex);

            usleep(100);//假装喝水
        }
        else
        {
            cout << (char*)arg << " 水已经满了 " << water << "\n";
            pthread_mutex_unlock(&mutex);
            //此处也需要加锁，否则break出去之后其他线程会因为没有解锁而挂起
            break;
        }
    }
    cout << (char*)arg << " 线程退出" << "\n";
    return (void*)0;
}

// 如果遇到2号信号，就在销毁锁后退出进程
void des(int signo)
{
    //销毁锁
    pthread_mutex_destroy(&mutex);
    cout << "pthread_mutex_destroy, exit" << endl;
    exit(0);
}

int main()
{
    signal(SIGINT,des);//自定义捕捉2号信号

    pthread_mutex_init(&mutex,nullptr);

    pthread_t t1,t2,t3,t4;//创建4个线程
    pthread_create(&t1,nullptr,func,(void*)"t1");
    pthread_create(&t2,nullptr,func,(void*)"t2");
    pthread_create(&t3,nullptr,func,(void*)"t3");
    pthread_create(&t4,nullptr,func,(void*)"t4");

    //直接分离线程
    pthread_detach(t1);
    pthread_detach(t2);
    pthread_detach(t3);
    pthread_detach(t4);

    while(1)
    {
        ;//啥都不干
    }

    return 0;
}