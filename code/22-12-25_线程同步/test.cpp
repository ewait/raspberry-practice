#include<iostream>
#include<string.h>
#include<signal.h>
#include<pthread.h>
#include<thread>
#include<unistd.h>
#include<sys/types.h>
#include<sys/syscall.h>
using namespace std;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;//锁
pthread_cond_t cond;//条件变量

volatile bool quit = false;


void*func1(void* arg)
{
    while(!quit)//这里有bug，无法集体退出
    {
        pthread_cond_wait(&cond,&mutex);//条件变量中会先解锁，条件满足后又重新获取锁
        cout << "thread is running... " << (char*)arg << endl;
        //其他线程无法退出，是因为我们申请了锁，线程退出之前又没有释放！
    }
    pthread_mutex_unlock(&mutex);//正确操作：需要在条件满足后，解锁
    cout << "thread quit... " << (char*)arg << endl;
}

int main()
{
    pthread_cond_init(&cond,nullptr);
    pthread_t t1,t2,t3;
    pthread_create(&t1,nullptr,func1,(void*)"t1");
    pthread_create(&t2,nullptr,func1,(void*)"t2");
    pthread_create(&t3,nullptr,func1,(void*)"t3");

    //pthread_mutex_unlock(&mutex);//如果一个线程没有锁却调用unlock，是没有用的（也不会报错）
    char c;
    while(1)
    {
        cout << "[a/b]$ ";
        cin >> c;
        if(c=='a')
        {
            pthread_cond_signal(&cond);
        }
        else if(c=='b')
        {
            pthread_cond_broadcast(&cond);
        }
        else
        {
            quit = true;
            break;
        }
        usleep(500);
    }

    cout << "main break: " << quit << endl;
    sleep(1);
    pthread_cond_broadcast(&cond);

    pthread_join(t1,nullptr);
    pthread_join(t2,nullptr);
    pthread_join(t3,nullptr);

    return 0;
}