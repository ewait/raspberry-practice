#include<iostream>
#include<queue>
#include<pthread.h>
#include<unistd.h>
using namespace std;

template<class T>
class BlockQueue
{
private:
    queue<T> _bq;//队列
    size_t _size;//大小
    pthread_mutex_t _mutex;//锁
    pthread_cond_t _proInf;//通知生产者
    pthread_cond_t _conInf;//通知消费者
public:
    BlockQueue(int sz=5)
        :_size(sz)
    {
        pthread_mutex_init(&_mutex,nullptr);
        pthread_cond_init(&_proInf,nullptr);
        pthread_cond_init(&_conInf,nullptr);
    }
    ~BlockQueue()
    {
        pthread_mutex_destroy(&_mutex);
        pthread_cond_destroy(&_proInf);
        pthread_cond_destroy(&_conInf);
    }
    //消费者消费
    T pop()
    {
        //加锁
        lock();
        //判断条件
        if(isEmpty())//空，不消费
        {
            ConWait();
        }

        //消费并通知生产者
        T tmp = _bq.front();
        _bq.pop();
        WakeUpPro();

        //解锁
        unlock();
        return tmp;
    }
    //生产者生产
    void push(const T& in)
    {
        //加锁
        lock();
        //判断条件
        if(isFull())//满，不生产
        {
            ProWait();
        }

        //生产并通知消费者
        _bq.push(in);
        WakeUpCon();

        //解锁
        unlock();
    }
private:
    void lock()
    {
        pthread_mutex_lock(&_mutex);
    }
    void unlock()
    {
        pthread_mutex_unlock(&_mutex);
    }
    //唤醒消费者
    void WakeUpCon()
    {
        pthread_cond_signal(&_conInf);
    }
    //唤醒生产者
    void WakeUpPro()
    {
        pthread_cond_signal(&_proInf);
    }
    //消费者等待
    void ConWait()
    {
        pthread_cond_wait(&_conInf,&_mutex);
    }
    //生产者等待
    void ProWait()
    {
        pthread_cond_wait(&_proInf,&_mutex);
    }

    //判断条件
    bool isFull()
    {
        return _size == _bq.size();
    }
    bool isEmpty()
    {
        return _bq.empty();
    }
};