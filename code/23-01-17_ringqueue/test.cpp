#include "ringqueue.hpp"
#include "task.hpp"
#include <time.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <string>

void *consume(void *args)
{
    RingQueue<int> *bqp = (RingQueue<int> *)args;
    while(1)
    {
        // 消费
        int ret = bqp->pop();
        cout << "consume " << pthread_self() << " 消费：" << ret << "\n";
        sleep(1);
    }
}
void *produce(void *args)
{
    RingQueue<int> *bqp = (RingQueue<int> *)args;
    while (1)
    {
        // 制作
        cout << "########↓" << "\n";
        int a = rand()%100;
        // 投放到超市
        bqp->push(a);
        cout << "produce " << pthread_self() << " 生产：" << a << "\n";
        sleep(1);
    }
}

string ops ="+-*/%";

void *consumer(void *args)
{
    RingQueue<Task> *bqp = (RingQueue<Task> *)args;
    while (1)
    {
        Task t = bqp->pop();
        int result = t();    //处理任务
        int one, two;
        char op;
        t.get(&one, &two, &op);
        cout << "consumer [" << pthread_self() << "] " << (unsigned long)time(nullptr) << " 消费了一个任务: " << one << op << two << " = " << result << endl;
    }
}
void *producer(void *args)
{
    RingQueue<Task> *bqp = (RingQueue<Task> *)args;
    while (1)
    {
        // 制作任务
        int one = rand() % 50;
        int two = rand() % 20;
        char op = ops[rand() % ops.size()];
        Task t(one, two, op);
        // 投放给消费者生产
        bqp->push(t);
        cout << "producter[" << pthread_self() << "] " << (unsigned long)time(nullptr) << " 生产了一个任务: " << one << op << two << " = ?" << endl;
        sleep(1);
    }
}

void test1()
{
    pthread_t t1,t2,t3,t4;
    RingQueue<int> bq(5);
    pthread_create(&t1,nullptr,produce,(void*)&bq);
    pthread_create(&t2,nullptr,consume,(void*)&bq);
    sleep(1);
    pthread_create(&t3,nullptr,produce,(void*)&bq);
    pthread_create(&t4,nullptr,consume,(void*)&bq);

    pthread_join(t1,nullptr);
    pthread_join(t2,nullptr);
    pthread_join(t3,nullptr);
    pthread_join(t4,nullptr);
}

void test2()
{
    pthread_t t1,t2;
    pthread_t t3,t4;
    RingQueue<Task> bq(5);
    pthread_create(&t1,nullptr,producer,(void*)&bq);
    sleep(1);
    pthread_create(&t2,nullptr,consumer,(void*)&bq);
    sleep(1);
    pthread_create(&t3,nullptr,producer,(void*)&bq);
    pthread_create(&t4,nullptr,consumer,(void*)&bq);

    pthread_join(t1,nullptr);
    pthread_join(t2,nullptr);
    pthread_join(t3,nullptr);
    pthread_join(t4,nullptr);
}

int main()
{
    srand((unsigned long)time(nullptr));//乘一个数字添加随机性
    test2();

    return 0;
}