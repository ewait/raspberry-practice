#pragma once

#include <iostream>
using namespace std;

class Task
{
public:
    Task() = default;
    Task(int one, int two, char op) 
        : _elem1(one), _elem2(two), _operator(op)
    {}
    // 仿函数
    int operator() ()
    {
        return run();
    }
    // 运行仿函数
    int run()
    {
        int result = 0;
        switch (_operator)
        {
        case '+':
            result = _elem1 + _elem2;
            break;
        case '-':
            result = _elem1 - _elem2;
            break;
        case '*':
            result = _elem1 * _elem2;
            break;
        case '/':
        {
            if (_elem2 == 0)
            {
                cout << "div zero, abort" << endl;
                result = -1;
            }
            else
            {
                result = _elem1 / _elem2;
            }
            break;
        } 
        case '%':
        {
            if (_elem2 == 0)
            {
                cout << "mod zero, abort" << endl;
                result = -1;
            }
            else
            {
                result = _elem1 % _elem2;
            }
            break;
        }
        default:
            cout << "unknown: " << _operator << endl;
            break;
        }
        return result;
    }

    // 获取元素，方便打印
    void get(int *e1, int *e2, char *op)
    {
        *e1 = _elem1;
        *e2 = _elem2;
        *op = _operator;
    }

    void resultPrint(int res)
    {
        cout << "[" << pthread_self() << "] 新线程完成计算任务: " << _elem1 << _operator << _elem2 << "=" << res << "\n";
    }

private:
    int _elem1;
    int _elem2;
    char _operator;
};