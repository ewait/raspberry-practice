#include "threadpool.hpp"
#include "task.hpp"
#include <string>
#include <time.h>

int main()
{
    prctl(PR_SET_NAME, "master");
    const string operators = "+/*/%";
    ThreadPool<Task>*tp = ThreadPool<Task>::getInstance();
    tp->start();

    srand((unsigned long)time(nullptr) ^ getpid() ^ pthread_self());
    // 派发任务
    while(1)
    {
        int one = rand()%50;
        int two = rand()%10;
        char oper = operators[rand()%operators.size()];
        cout << "[" << pthread_self() << "] 主线程派发计算任务: " << one << oper << two << "=?" << "\n";
        Task t(one, two, oper);
        tp->push(t);
        sleep(1);
    }
    
}