#include <iostream>
#include <unistd.h>
#include <pthread.h>
using namespace std;

volatile int board = 0;//临界资源

pthread_rwlock_t rw;//全局读写锁

void *reader(void* args)
{
    const char *name = static_cast<const char *>(args);
    cout << "reader ["<<pthread_self() <<"]"<< endl;
    //sleep(1);
    while(true)
    {
        pthread_rwlock_rdlock(&rw);
        cout << "reader ["<<pthread_self() <<"] " << board << endl;
        sleep(10);
        pthread_rwlock_unlock(&rw);
        usleep(110);
    }
}

void *writer(void *args)
{
    const char *name = static_cast<const char *>(args);
    //sleep(1);
    while(true)
    {
        pthread_rwlock_wrlock(&rw);
        board++;
        cout << "writer [" << pthread_self() <<"]"<< endl;
        //sleep(10);
        pthread_rwlock_unlock(&rw);
        usleep(100);
    }
}

int main()
{
    // pthread_rwlockattr_t attr;//属性
    // pthread_rwlockattr_init(&attr);//初始化属性
    // pthread_rwlockattr_setkind_np(&attr, PTHREAD_RWLOCK_PREFER_WRITER_NONRECURSIVE_NP);//设置锁的属性为写者优先
    // pthread_rwlock_init(&rw, &attr);//初始化并设置读写锁的属性
    pthread_rwlock_init(&rw, nullptr);
    pthread_t r1,r2,r3,r4,r5,r6, w1,w2;
    pthread_create(&r1, nullptr, reader, (void*)"reader");
    pthread_create(&r2, nullptr, reader, (void*)"reader");
    pthread_create(&r3, nullptr, reader, (void*)"reader");
    pthread_create(&r4, nullptr, reader, (void*)"reader");
    pthread_create(&r5, nullptr, reader, (void*)"reader");
    pthread_create(&r6, nullptr, reader, (void*)"reader");
    pthread_create(&w1, nullptr, writer, (void*)"writer");
    pthread_create(&w2, nullptr, writer, (void*)"writer");


    pthread_join(r1, nullptr);
    pthread_join(r2, nullptr);
    pthread_join(r3, nullptr);
    pthread_join(r4, nullptr);
    pthread_join(r5, nullptr);
    pthread_join(r6, nullptr);
    pthread_join(w1, nullptr);
    pthread_join(w2, nullptr);

    pthread_rwlock_destroy(&rw);
    return 0;
}
