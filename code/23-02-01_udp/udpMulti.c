#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define MCAST_GROUP "224.0.0.1"
#define MCAST_PORT  5007

int main()
{
    int sockfd;
    struct sockaddr_in addr;
    socklen_t addrlen = sizeof(addr);
    char message[] = "Hello, Multicast World!";
    char buffer[1024];

    // 创建UDP套接字
    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    // 设置套接字的地址和端口
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY);  // 绑定到所有可用的网络接口
    addr.sin_port = htons(MCAST_PORT);

    // 绑定套接字
    if (bind(sockfd, (struct sockaddr *)&addr, sizeof(addr)) == -1)
    {
        perror("bind");
        exit(EXIT_FAILURE);
    }

    // 加入多播组
    struct ip_mreq mreq;
    mreq.imr_multiaddr.s_addr = inet_addr(MCAST_GROUP);
    mreq.imr_interface.s_addr = htonl(INADDR_ANY);
    if (setsockopt(sockfd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) == -1)
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    // 发送数据到多播组
    if (sendto(sockfd, message, strlen(message), 0, (struct sockaddr *)&addr, sizeof(addr)) == -1)
    {
        perror("sendto");
        exit(EXIT_FAILURE);
    }

    // 从多播组里面接收数据
    if (recvfrom(sockfd, buffer, sizeof(buffer), 0, (struct sockaddr *)&addr, &addrlen) == -1)
    {
        perror("recvfrom");
        exit(EXIT_FAILURE);
    }

    // 发送数据到普通ip端口
    struct sockaddr_in server;
    bzero(&server, sizeof(server));                   //这个函数相当于memset全0
    server.sin_family = AF_INET;                      //ipv4
    server.sin_port = htons(8080);                    //目标服务器端口
    server.sin_addr.s_addr = inet_addr("127.0.0.1");  //目标ip

    if (sendto(sockfd, buffer, sizeof(buffer), 0, (const struct sockaddr *)&server, sizeof(server)) == -1)
    {
        perror("sendto");
        exit(EXIT_FAILURE);
    }

    // 打印接收到的数据
    printf("Received: %s\n", buffer);

    // 关闭套接字
    close(sockfd);

    return 0;
}
