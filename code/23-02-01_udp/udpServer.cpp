#include <iostream>
#include <string>
#include <map>
#include <cstring>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
using namespace std;

#include "log.hpp"
#define BUF_SIZE 1024


class UdpServer
{
public:
    UdpServer(uint16_t port,const string& ip="")
     : _port((uint16_t)port), _ip(ip), _sockfd(-1)
    {
        // 1.创建socket套接字
        _sockfd = socket(AF_INET, SOCK_DGRAM, 0); //本质是打开了一个文件
        if (_sockfd < 0)
        {
            logging(FATAL, "socket:%s:%d", strerror(errno), _sockfd);
            exit(1);
        }
        logging(DEBUG, "socket create success: %d", _sockfd);
        // 2. 绑定网络信息，指明ip+port
        // 2.1 先填充基本信息到 struct sockaddr_in
        struct sockaddr_in local;
        memset(&local,0,sizeof(local));//初始化
        // 协议家族，设置为ipv4
        local.sin_family = AF_INET; 
        // 端口，需要进行 本地->网络转换
        local.sin_port = htons(_port);
        // 配置ip
        // 如果初始化时候的ip为空，则调用INADDR_ANY代表任意ip。否则对传入的ip进行转换后赋值
        local.sin_addr.s_addr = _ip.empty() ? htonl(INADDR_ANY) : inet_addr(_ip.c_str());

        // 2.2 绑定ip端口
        if (bind(_sockfd,(const struct sockaddr *)&local, sizeof(local)) == -1)
        {
            logging(FATAL, "bind: %s:%d", strerror(errno), _sockfd);
            exit(2);
        }
        logging(DEBUG,"socket bind success: %d", _sockfd);
    }

    void start()
    {
        char inBuf[BUF_SIZE];//接收到信息的缓冲区
        while(1)
        {
            struct sockaddr_in peer;      //输出型参数
            socklen_t len = sizeof(peer); //输入输出型参数

            // peer和len都是输出型参数，用于获取发送方的信息
            // len是输入输出型参数，需要以sizeof(peer)初始化后传入
            // 第三个参数0为默认等待方式（阻塞等待）
            ssize_t s = recvfrom(_sockfd, inBuf, sizeof(inBuf)-1,0,(struct sockaddr *)&peer, &len);
            if (s > 0) // s代表获取到的数据长度，不为0代表成功获取
            {
                inBuf[s] = '\0'; //末尾追加'\0'
            }
            else if (s == -1) // -1没有收到信息，错误
            {
                logging(WARINING, "recvfrom: %s:%d", strerror(errno), _sockfd);
                continue;
            }
            
            string senderIP = inet_ntoa(peer.sin_addr);// 来源ip
            uint16_t senderPort = ntohs(peer.sin_port); // 来源端口
            
            // 记录用户
            CheckUser(peer);
            // 消息路由（把收到的消息重发给所有用户，相当于一个简单的聊天室）
            MsgRoute(peer,inBuf,s);
            // 打印信息
            logging(NOTICE, "[%s:%d]# %s", senderIP.c_str(),senderPort, inBuf);
        }
    }

    void CheckUser(struct sockaddr_in peer)
    {
        string tmp = inet_ntoa(peer.sin_addr);// 来源ip
        tmp += ':';
        tmp += to_string(ntohs(peer.sin_port));// 来源端口

        // 在map中用ip端口来标识用户
        auto it = _usrMap.find(tmp);
        if(it == _usrMap.end())// 没找到
        {
            _usrMap.insert({tmp,peer});
        }
    }

    void MsgRoute(struct sockaddr_in peer,const char* inBuf,size_t len)
    {
        struct sockaddr_in user;
        for(auto e:_usrMap)
        {
            // 如果ip和端口都相等，就代表是发送消息的用户
            if(e.second.sin_port != peer.sin_port || e.second.sin_addr.s_addr != peer.sin_addr.s_addr)
            {
                user.sin_family = AF_INET;//ipv4
                user.sin_port = e.second.sin_port;//用户端口
                user.sin_addr.s_addr = e.second.sin_addr.s_addr;//用户ip
                // 向用户发送信息
                sendto(_sockfd, inBuf, len, 0,
                (const struct sockaddr *)&user, sizeof(user)); 
            }
        }
    }

    ~UdpServer()
    {
        close(_sockfd);
    }
private:
    // 服务器端口号
    uint16_t _port;
    // 服务器ip地址
    string _ip;
    // 服务器socket fd信息
    int _sockfd;
    // 存放用户信息
    map<string,struct sockaddr_in> _usrMap;
};

int main(int argc,char* argv[])
{
    //参数只有两个（端口/ip）所以参数个数应该是2-3
    if(argc!=2 && argc!=3)
    {
        cout << "Usage: " << argv[0] << " port [ip]" << endl;
        return 1;
    }
    

    string ip;
    // 3个参数，有ip
    if(argc==3)
    {
        ip = argv[2];
    }
    UdpServer s(atoi(argv[1]),ip);
    s.start();

    return 0;
}