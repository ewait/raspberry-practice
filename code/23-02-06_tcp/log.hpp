#pragma once

#include <cstdio>
#include <ctime>
#include <cstdarg>
#include <cassert>
#include <cstring>
#include <cerrno>
#include <stdlib.h>

#define DEBUG 0
#define NOTICE 1
#define WARINING 2
#define FATAL 3

const char *log_level[]={"DEBUG", "NOTICE", "WARINING", "FATAL"};

// 采用可变参数列表
void logging(int level, const char *format, ...)
{
    assert(level >= DEBUG || level <= FATAL);

    char *name = getenv("USER");// 获取环境变量中的用户（执行命令的用户）

    char logInfo[1024];
    // 获取可变参数列表
    va_list ap; // ap -> char*
    va_start(ap, format);

    vsnprintf(logInfo, sizeof(logInfo)-1, format, ap);

    va_end(ap); // ap = NULL

    // 根据日志等级选择打印到stderr/stdout
    FILE *out = (level == FATAL) ? stderr:stdout;
    // 格式化打印到文件中
    fprintf(out, "%s | %u | %s | %s\n", \
        log_level[level], \
        (unsigned int)time(nullptr),\
        name == nullptr ? "unknow":name,\
        logInfo);
}