#include "utils.h"


// ./clientTcp serverIp serverPort
int main(int argc, char *argv[])
{
    if (argc != 3)//客户端必须要有3个参数
    {
        cerr << "Usage:\n\t" << argv[0] << " serverIp serverPort" << endl;
        cerr << "Example:\n\t" << argv[0] << " 127.0.0.1 8080\n"
                << endl;
        exit(USAGE_ERR);
    }
    // 解析服务端的ip和端口
    string serverIp = argv[1];
    uint16_t serverPort = atoi(argv[2]);

    // 1. 创建tcp的socket SOCK_STREAM
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0)
    {
        cerr << "socket: " << strerror(errno) << endl;
        exit(SOCKET_ERR);
    }

    // 2. connect，发起链接请求，你想谁发起请求呢？？当然是向服务器发起请求喽
    // 2.1 先填充需要连接的远端主机的基本信息
    struct sockaddr_in server;
    memset(&server, 0, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_port = htons(serverPort);
    inet_aton(serverIp.c_str(), &server.sin_addr);
    // 2.2 发起请求，connect 会自动bind
    if (connect(sock, (const struct sockaddr *)&server, sizeof(server)) != 0)
    {
        cerr << "connect: " << strerror(errno) << endl;
        exit(CONN_ERR);
    }
    cout << "connect success: " << sock << endl;

    // 客户端发现的消息
    string message;
    while (1)
    {
        message.clear();//每次循环开始，都清空一下msg
        cout << "请输入你的消息# ";
        getline(cin, message);//获取输入
        // 如果客户端输入了quit，则退出
        if (strcasecmp(message.c_str(), "quit") == 0)
            break;
        // 向服务端发送消息
        ssize_t s = write(sock, message.c_str(), message.size());
        if (s > 0) // 写入成功
        {
            message.clear();//清空输入的消息
            message.resize(BUFFER_SIZE);
            // 因为string的c_str本质上是返回地址，所以强转后是可以往里面写入的
            s = read(sock, (char *)(message.c_str()), BUFFER_SIZE);// 获取服务端的结果
            if (s > 0)// 读取成功
            {
                message[s] = '\0';//追加\0
            }
            // 打印返回值
            cout << "Server Echo# " << message << endl;
        }
        else if (s <= 0) // 写入失败
        {
            break;
        }
    }
    // 关闭文件描述符
    close(sock);
    return 0;
}