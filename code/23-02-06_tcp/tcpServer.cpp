#include "utils.h"
#include "log.hpp"
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <pthread.h>

class TcpServer;

struct ClientData
{
    int _fd;
    uint16_t _port;
    string _ip;
    TcpServer* _this;

    ClientData(int fd,uint16_t port,const string& ip,TcpServer* this1)
        :_fd(fd),_port(port), _ip(ip),_this(this1)
    {}
};

class TcpServer
{
public:
    TcpServer(uint16_t port,const string& ip="")
     :_port(port), _ip(ip), _listenSock(-1)
    {
        // 1.创建socket套接字,采用字节流（即tcp）
        _listenSock = socket(AF_INET, SOCK_STREAM, 0); //本质是打开了一个文件
        if (_listenSock < 0)
        {
            logging(FATAL, "socket:%s:%d", strerror(errno), _listenSock);
            exit(1);
        }
        logging(DEBUG, "socket create success: %d", _listenSock);

        // 2. 绑定网络信息，指明ip+port
        // 2.1 先填充基本信息到 struct sockaddr_in
        struct sockaddr_in local;
        memset(&local,0,sizeof(local));//初始化
        // 协议家族，设置为ipv4
        local.sin_family = AF_INET; 
        // 端口，需要进行 本地->网络转换
        local.sin_port = htons(_port);
        // 配置ip
        // 如果初始化时候的ip为空，则调用INADDR_ANY代表任意ip。否则对传入的ip进行转换后赋值
        local.sin_addr.s_addr = _ip.empty() ? htonl(INADDR_ANY) : inet_addr(_ip.c_str());
        // 2.2 绑定ip端口
        if (bind(_listenSock,(const struct sockaddr *)&local, sizeof(local)) == -1)
        {
            logging(FATAL, "bind: %s:%d", strerror(errno), _listenSock);
            exit(2);
        }
        logging(DEBUG,"socket bind success: %d", _listenSock);
        // 3.监听
        // tcp服务器是需要连接的，连接之前要先监听有没有人来连
        if (listen(_listenSock, 5) < 0)
        {
            logging(FATAL, "listen: %s", strerror(errno));
            exit(LISTEN_ERR);
        }
        logging(DEBUG, "listen: %s, %d", strerror(errno), _listenSock);
    }

    ~TcpServer()
    {// 关闭文件描述符
        close(_listenSock);
    }

    void start()
    {
        //signal(SIGCHLD, FreeChild);
        while(1)
        {
            struct sockaddr_in peer;
            socklen_t len = sizeof(peer);
            // 获取连接
            int conet = accept(_listenSock,(struct sockaddr*)&peer,&len);
            if(conet<0)
            {
                logging(FATAL, "accept: %s", strerror(errno));
                exit(CONN_ERR);//连接错误
            }
            // 获取连接信息
            string senderIP = inet_ntoa(peer.sin_addr);// 来源ip
            uint16_t senderPort = ntohs(peer.sin_port); // 来源端口
            logging(DEBUG, "accept: %s | %s[%d], socket fd: %d", strerror(errno), senderIP.c_str(), senderPort, conet);

            // // 提供服务(线程)
            // pthread_t service;
            // // 因为这个成员使用范围极小，所以采用new/delete，避免占用太多空间
            // ClientData* data = new ClientData(conet,senderPort,senderIP,this);
            // pthread_create(&service,nullptr,threadRoutine,(void*)data);

            // // 提供服务（子进程）-1
            // pid_t id = fork();
            // if(id == 0)
            // {
            //     close(_listenSock);//因为子进程不需要监听，所以关闭掉监听socket
            //     //子进程
            //     transService(conet, senderIP, senderPort);
            //     exit(0);// 服务结束后，退出，子进程会进入僵尸状态等待父进程回收
            // }
            // // 父进程
            // close(conet); // 因为此时是子进程提供服务，conet会有拷贝，相当于有两个进程打开了该文件
            // // 如果父进程不关闭，即便子进程结束服务了，该文件描述符也会保持开启

            // 提供服务（孙子进程）-2
            pid_t id = fork();
            if(id == 0)
            {
                close(_listenSock);//因为子进程不需要监听，所以关闭掉监听socket
                //又创建一个子进程，大于0代表是父进程，即创建完子进程后父进程直接退出
                if(fork()>0){
                    exit(0);
                }
                    
                // 孙子进程执行
                transService(conet, senderIP, senderPort);
                exit(0);// 服务结束后，退出，子进程会进入僵尸状态等待父进程回收
            }
            // 爷爷进程
            close(conet); 
            pid_t ret = waitpid(id, nullptr, 0); //此时就可以直接用阻塞式等待了
            assert(ret > 0);//ret如果不大于0，则代表等待发生了错误
        }
    }
    // 因为需要取消this指针，所以需要设置成静态的
    static void* threadRoutine(void*args)
    {   
        pthread_detach(pthread_self()); //设置线程分离
        ClientData* data=(ClientData*)args;
        // 通过预先设置的this指针来访问类内成员，并进行传参
        data->_this->transService(data->_fd,data->_ip,data->_port);
        delete data;
        return nullptr;
    }

    void transService(int sockfd, const string &clientIp, uint16_t clientPort)
    {
        assert(sockfd >= 0);
        assert(!clientIp.empty());
        assert(clientPort>0);
        // 开始服务
        char buf[BUFFER_SIZE];
        while(1)
        {
            // 读取客户端发来的信息,s是读取到的字节数
            ssize_t s = read(sockfd, buf, sizeof(buf)-1);
            if(s>0)
            {
                buf[s]='\0';//手动添加字符串终止符
                if(strcasecmp(buf,"quit")==0)
                {//客户端主动退出
                    break;
                }
                // 服务
                string tmp = buf;
                int ret = str2ascii(tmp);//获取字符串的ascii总和
                string retS =  to_string(ret);//转字符串
                write(sockfd,retS.c_str(),retS.size());//写入
            }
	        else if (s == 0)
            {//s == 0代表对方关闭,客户端退出
                logging(DEBUG, "client quit: %s[%d]", clientIp.c_str(), clientPort);
                break;
            }
            else
            {
                logging(DEBUG, "read err: %s[%d] -  %s", clientIp.c_str(), clientPort, strerror(errno));
                continue;
            }
        }
        close(sockfd);
        logging(DEBUG,"server quit %d",sockfd);
    }

private:
    // 服务函数可以不暴露
    int str2ascii(const string& str)
    {
        int ret = 0;
        for(auto e:str)
        {
            ret += e;
        }
        return ret;
    }
    // 回收子进程
    void FreeChild(int signo)
    {
        assert(signo == SIGCHLD);
        while (true)
        {
            //如果没有子进程了，waitpid就会调用失败
            pid_t id = waitpid(-1, nullptr, WNOHANG); // 非阻塞等待
            if (id > 0)
            {
                cout << "父进程等待成功, child pid: " << id << endl;
            }
            else if(id == 0)
            {
                //还有子进程没有退出
                cout << "尚有未退出的子进程，父进程继续运行" << endl;
                break;//退出等待子进程
            }
            else
            {
                cout << "父进程等待所有子进程结束" << endl;
                break;
            }
        }
    }

    // 服务器端口号
    uint16_t _port;
    // 服务器ip地址
    string _ip;
    // 服务器socket fd信息
    int _listenSock;
};


int main(int argc,char* argv[])
{
    //参数只有两个（端口/ip）所以参数个数应该是2-3
    if(argc!=2 && argc!=3)
    {
        cout << "Usage: " << argv[0] << " port [ip]" << endl;
        return 1;
    }
    

    string ip;
    // 3个参数，有ip
    if(argc==3)
    {
        ip = argv[2];
    }
    TcpServer t(atoi(argv[1]),ip);
    t.start();

    return 0;
}