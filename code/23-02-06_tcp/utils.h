#pragma once
// 头文件太多了，所以新起一个文件
#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <cassert>
#include <ctype.h>//判断字符串大写小写接口需要的库
#include <unistd.h> 
#include <strings.h>// 忽略大小写比较strcasecmp
#include <sys/types.h>  //很多liunx系统接口都需要这个
#include <sys/socket.h> // 网络
#include <netinet/in.h> // 网络
#include <arpa/inet.h> // 网络
using namespace std;

#define SOCKET_ERR 1
#define BIND_ERR   2
#define LISTEN_ERR 3
#define USAGE_ERR  4
#define CONN_ERR   5

#define BUFFER_SIZE 1024
