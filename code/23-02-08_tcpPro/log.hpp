#pragma once

#include <cstdio>
#include <ctime>
#include <cstdarg>
#include <cassert>
#include <cstring>
#include <cerrno>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#define DEBUG 0
#define NOTICE 1
#define WARINING 2
#define FATAL 3

const char *log_level[]={"DEBUG", "NOTICE", "WARINING", "FATAL"};

#define LOG_PATH "./log.txt" //工作路径下的log.txt

// 这个类只用于重定向，不需要在里面加其他东西
class Logdup
{
public:
    Logdup()
        :_fdout(-1),_fderr(-1)
    {}
    Logdup(const char* pout=LOG_PATH,const char* perr="")
        :_fdout(-1),_fderr(-1)
    {
        //如果只传入了第一个pout，则代表将perr和pout重定向为一个路径
        umask(0);
        int logfd = open(pout, O_WRONLY | O_CREAT | O_APPEND, 0666);
        assert(logfd != -1);
        _fdout = _fderr = logfd;//赋值可以连等
        //判断是不是空串
        if(strcmp(perr,"")!=0)//不相同，代表单独设置了err的路径
        {
            logfd = open(perr, O_WRONLY | O_CREAT | O_APPEND, 0666);
            assert(logfd != -1);
            _fderr = logfd;
        }
        dup2(_fdout, 1);//重定向stdout
        dup2(_fderr, 2);//重定向stderr
    }

    ~Logdup()
    {
        if(_fdout!= -1)
        {
            fsync(_fdout);
            fsync(_fderr);
            // 先写盘再关闭
            close(_fdout);
            close(_fderr);
        }
    }
private:
    int _fdout;//重定向的日志文件描述符
    int _fderr;//重定向的错误文件描述符
};

// 采用可变参数列表
void logging(int level, const char *format, ...)
{
    assert(level >= DEBUG || level <= FATAL);

    char *name = getenv("USER");// 获取环境变量中的用户（执行命令的用户）

    char logInfo[1024];
    // 获取可变参数列表
    va_list ap; // ap -> char*
    va_start(ap, format);

    vsnprintf(logInfo, sizeof(logInfo)-1, format, ap);

    va_end(ap); // ap = NULL

    // 根据日志等级选择打印到stderr/stdout
    FILE *out = (level == FATAL) ? stderr:stdout;
    // 格式化打印到文件中
    fprintf(out, "%s | %u | %s | %s\n", \
        log_level[level], \
        (unsigned int)time(nullptr),\
        name == nullptr ? "unknow":name,\
        logInfo);

    fflush(out); // 将C缓冲区中的数据刷新到OS
    fsync(fileno(out));// 将OS中的数据写入硬盘
}