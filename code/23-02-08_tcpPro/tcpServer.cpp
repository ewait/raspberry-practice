#include "utils.h"
#include "log.hpp"
#include "threadpool.hpp"
#include "task.hpp"
#include "daemons.hpp"
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <pthread.h>
using namespace std;

class TcpServer;

struct ClientData
{
    int _fd;
    uint16_t _port;
    string _ip;
    TcpServer* _this;

    ClientData(int fd,uint16_t port,const string& ip,TcpServer* this1)
        :_fd(fd),_port(port), _ip(ip),_this(this1)
    {}
};

class TcpServer
{
public:
    TcpServer(uint16_t port,const string& ip="")
     :_port(port), _ip(ip), _listenSock(-1)
    {
        // 1.创建socket套接字,采用字节流（即tcp）
        _listenSock = socket(AF_INET, SOCK_STREAM, 0); //本质是打开了一个文件
        if (_listenSock < 0)
        {
            logging(FATAL, "socket:%s:%d", strerror(errno), _listenSock);
            exit(1);
        }
        logging(DEBUG, "socket create success: %d", _listenSock);

        // 2. 绑定网络信息，指明ip+port
        // 2.1 先填充基本信息到 struct sockaddr_in
        struct sockaddr_in local;
        memset(&local,0,sizeof(local));//初始化
        // 协议家族，设置为ipv4
        local.sin_family = AF_INET; 
        // 端口，需要进行 本地->网络转换
        local.sin_port = htons(_port);
        // 配置ip
        // 如果初始化时候的ip为空，则调用INADDR_ANY代表任意ip。否则对传入的ip进行转换后赋值
        local.sin_addr.s_addr = _ip.empty() ? htonl(INADDR_ANY) : inet_addr(_ip.c_str());
        // 2.2 绑定ip端口
        if (bind(_listenSock,(const struct sockaddr *)&local, sizeof(local)) == -1)
        {
            logging(FATAL, "bind: %s:%d", strerror(errno), _listenSock);
            exit(2);
        }
        logging(DEBUG,"socket bind success: %d", _listenSock);
        // 3.监听
        // tcp服务器是需要连接的，连接之前要先监听有没有人来连
        if (listen(_listenSock, 5) < 0)
        {
            logging(FATAL, "listen: %s", strerror(errno));
            exit(LISTEN_ERR);
        }
        logging(DEBUG, "listen: %s, %d", strerror(errno), _listenSock);

        // 4.获取线程池 单例
        _tpool = ThreadPool<Task>::getInstance(2);
        _tpool->start();//开始运行
    }

    ~TcpServer()
    {// 关闭文件描述符
        close(_listenSock);
    }

    void start()
    {
        while(1)
        {
            struct sockaddr_in peer;
            socklen_t len = sizeof(peer);
            // 获取连接
            int conet = accept(_listenSock,(struct sockaddr*)&peer,&len);
            if(conet<0)
            {
                logging(FATAL, "accept: %s", strerror(errno));
                exit(CONN_ERR);//连接错误
            }
            // 获取连接信息
            string senderIP = inet_ntoa(peer.sin_addr);// 来源ip
            uint16_t senderPort = ntohs(peer.sin_port); // 来源端口
            logging(DEBUG, "accept: %s | %s[%d], socket fd: %d", strerror(errno), senderIP.c_str(), senderPort, conet);

            // 提供服务（通过线程池）
            Task t(conet,senderIP,senderPort,transService);
            _tpool->push(t);
        }
    }
private:
    // 服务器端口号
    uint16_t _port;
    // 服务器ip地址
    string _ip;
    // 服务器socket fd信息
    int _listenSock;
    // 线程池
    ThreadPool<Task>* _tpool;
};


int main(int argc,char* argv[])
{
    //参数只有两个（端口/ip）所以参数个数应该是2-3
    if(argc!=2 && argc!=3)
    {
        cout << "Usage: " << argv[0] << " port [ip]" << endl;
        return 1;
    }
    daemonize();// 守护进程化
    Logdup l("./log.txt");//初始化log重定向的路径

    string ip;
    // 3个参数，有ip
    if(argc==3)
    {
        ip = argv[2];
    }
    TcpServer t(atoi(argv[1]),ip);
    t.start();

    return 0;
}