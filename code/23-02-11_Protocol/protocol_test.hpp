#include"protocol.h"
#include<iostream>
using namespace std;

int main()
{
    string s = "101/ 23";
    bool ret = true;
    Request r(s,&ret);
    if(!ret)
    {
        cout << "err " << ret << endl;
    }
    r.debug();
    string buf;
    r.serialize(buf);
    cout << buf << endl;
    string tmp = encode(buf,buf.size());
    cout << tmp << endl;

    // 反序列化测试
    tmp = "9\t300 + 200\t";
    size_t sz = tmp.size();
    string dc = decode(tmp,&sz);
    if(sz==0){
        cout << "Err " << sz << endl;
    }
    ret = r.deserialize(dc);
    cout << ret << endl;
    cout << dc << endl;
    r.debug();

    return 0;
}