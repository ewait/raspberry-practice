#include "utils.h"
#include "log.hpp"
#include "protocol.hpp"
#include <functional>

int str2ascii(const string &str)
{
    int ret = 0;
    for (auto e : str)
    {
        ret += e;
    }
    return ret;
}

void transService(int sockfd, const std::string &clientIP, uint16_t clientPort)
{
    assert(sockfd >= 0);
    assert(!clientIP.empty());
    assert(clientPort > 0);
    // 开始服务
    char buf[BUFFER_SIZE];
    while (1)
    {
        // 读取客户端发来的信息,s是读取到的字节数
        ssize_t s = read(sockfd, buf, sizeof(buf) - 1);
        if (s > 0)
        {
            buf[s] = '\0'; // 手动添加字符串终止符
            if (strcasecmp(buf, "quit") == 0)
            { // 客户端主动退出
                break;
            }
            // 服务
            std::string tmp = buf;
            int ret = str2ascii(tmp);                 // 获取字符串的ascii总和
            std::string retS = std::to_string(ret);   // 转字符串
            write(sockfd, retS.c_str(), retS.size()); // 写入
        }
        else if (s == 0)
        {   // s == 0代表对方发送了空消息，视作客户端主动退出
            logging(DEBUG, "client quit: %s[%d]", clientIP.c_str(), clientPort);
            break;
        }
        else
        {
            // 出现了读取错误，打印日志后断开连接
            logging(DEBUG, "read err: %s[%d] -  %s", clientIP.c_str(), clientPort, strerror(errno));
            break;
        }
    }
    close(sockfd);
    logging(DEBUG, "server quit %d", sockfd);
}


Response Caculater(const Request& req)
{
    Response resp;
    switch (req._ops)
    {
    case '+':
        resp._result = req._x + req._y;
        break;
    case '-':
        resp._result = req._x - req._y;
        break;
    case '*':
        resp._result = req._x * req._y;
        break;
    case '%':
    {
        if(req._y == 0)
        {
            resp._exitCode = -1;//取模错误
            break;
        }
        resp._result = req._x % req._y;//取模是可以操作负数的
        break;
    }
    case '/':
    {
        if(req._y == 0)
        {
            resp._exitCode = -2;//除0错误
            break;
        }
        resp._result = req._x / req._y;//取模是可以操作负数的
        break;
    }
    default:
        resp._exitCode = -3;//操作符非法
        break;
    }

    return resp;
}


void CaculateService(int sockfd, const std::string &clientIP, uint16_t clientPort)
{
    assert(sockfd >= 0);
    assert(!clientIP.empty());
    assert(clientPort > 0);

    std::string inbuf;
    while(1)
    {
        Request req;
        char buf[BUFFER_SIZE];
        // 1.读取客户端发送的信息
        ssize_t s = read(sockfd, buf, sizeof(buf) - 1);
        if (s == 0)
        {   // s == 0代表对方发送了空消息，视作客户端主动退出
            logging(DEBUG, "client quit: %s[%d]", clientIP.c_str(), clientPort);
            break;
        }
        else if(s<0)
        {
            // 出现了读取错误，打印日志后断开连接
            logging(DEBUG, "read err: %s[%d] = %s", clientIP.c_str(), clientPort, strerror(errno));
            break;
        }
        // 2.读取成功
        buf[s] = '\0'; // 手动添加字符串终止符
        if (strcasecmp(buf, "quit") == 0)
        { // 客户端主动退出
            break;
        }
        // 3.开始服务
        inbuf = buf;
        size_t packageLen = inbuf.size();
        // 3.1.解码和反序列化客户端传来的消息
        std::string package = decode(inbuf, &packageLen);//解码
        if(packageLen==0){
            logging(DEBUG, "decode err: %s[%d] status: %d", clientIP.c_str(), clientPort, packageLen);
            continue;//报文不完整或有误
        }
        logging(DEBUG,"package: %s[%d] = %s",clientIP.c_str(), clientPort,package.c_str());
        bool deStatus = req.deserialize(package); // 反序列化
        if(deStatus) // 获取消息反序列化成功
        {
            req.debug(); // 打印信息
            // 3.2.获取结构化的相应
            Response resp = Caculater(req);
            // 3.3.序列化和编码响应
            std::string echoStr;
            resp.serialize(echoStr);
            echoStr = encode(echoStr,echoStr.size());
            // 3.4.写入，发送返回值给客户端
            write(sockfd, echoStr.c_str(), echoStr.size());
        }
        else // 客户端消息反序列化失败
        {
            logging(DEBUG, "deserialize err: %s[%d] status: %d", clientIP.c_str(), clientPort, deStatus);
            continue;
        }
    }
    close(sockfd);
    logging(DEBUG, "server quit: %s[%d] %d",clientIP.c_str(), clientPort, sockfd);
}


class Task
{
    using callback_t = std::function<void (int, std::string, uint16_t)>;//相当于typedef
public:
    Task() = default;
    // 将需要调用的函数传入，相当于通用
    Task(int sockfd, const std::string &clientIP, uint16_t clientPort,callback_t func)
        : _sockfd(sockfd), _ip(clientIP), _port(clientPort),_func(func)
    {
    }
    // 仿函数
    void operator()()
    {
        logging(DEBUG, "TID[%p] = %s:%d START",\
            pthread_self(), _ip.c_str(), _port);

        _func(_sockfd, _ip, _port);

        logging(DEBUG, "TID[%p] = %s:%d END  ",\
            pthread_self(), _ip.c_str(), _port);
    }

private:
    int _sockfd;
    std::string _ip;
    uint16_t _port;
    callback_t _func;
};