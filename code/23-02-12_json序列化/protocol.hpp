#pragma once

#include "utils.h"
#include <jsoncpp/json/json.h>
//#define MYPROTOCOL 1  //如果define了这个，那就使用自己的代码

#define CRLF "\t"   //分隔符
#define CRLF_LEN strlen(CRLF) //分隔符长度
#define SPACE " "   //空格
#define SPACE_LEN strlen(SPACE) //空格长度

#define OPS "+-*/%" //运算符

//编码在请求、回复中都是通用的
//参数len为in的长度，是一个输出型参数。如果为0代表err
std::string decode(std::string& in,size_t*len)
{
    assert(len);//如果长度为0是错误的
    // 1.确认in的序列化字符串完整（分隔符）
    *len=0;
    size_t pos = in.find(CRLF);//查找分隔符
    //查找不到，err
    if(pos == std::string::npos){
        return "";//返回空串
    }   
    // 2.有分隔符，判断长度是否达标
    // 此时pos下标正好就是标识大小的字符长度
    std::string inLenStr = in.substr(0,pos);//提取字符串长度
    size_t inLen = atoi(inLenStr.c_str());//转int
    size_t left = in.size() - inLenStr.size()- 2*CRLF_LEN;//剩下的字符长度
    if(left<inLen){
        return ""; //剩下的长度没有达到标明的长度
    }
    // 3.走到此处，字符串完整，开始提取序列化字符串
    std::string ret = in.substr(pos+CRLF_LEN,inLen);
    *len = inLen;
    // 4.因为in中可能还有其他的报文（下一条）
    // 所以需要把当前的报文从in中删除，方便下次decode，避免二次读取
    size_t rmLen = inLenStr.size() + ret.size() + 2*CRLF_LEN;
    in.erase(0,rmLen);
    // 5.返回
    return ret;
}

//编码不需要修改源字符串，所以const。参数len为in的长度
std::string encode(const std::string& in,size_t len)
{
    std::string ret = std::to_string(len);//将长度转为字符串添加在最前面，作为标识
    ret+=CRLF;
    ret+=in;
    ret+=CRLF;
    return ret;
}

// 自定义协议的请求
class Request
{
public:
    Request() = default;
    // 将用户的输入转成内部成员
    // 用户可能输入x+y，x+ y，x +y,x + y等等格式
    // 提前修改用户输入（主要还是去掉空格），提取出成员
    Request(std::string in,bool* status)
        :_x(0),_y(0),_ops(' ')
    {
        rmSpace(in);
        // 这里使用c的字符串，因为有strtok
        char buf[1024];
        // 打印n个字符，多的会被截断
        snprintf(buf,sizeof(buf),"%s",in.c_str());
        char* left = strtok(buf,OPS);
        if(!left){//找不到
            *status = false;
            return;
        }
        char*right = strtok(nullptr,OPS);
        if(!right){//找不到
            *status = false;
            return;
        }
        // x+y, strtok会将+设置为\0
        char mid = in[strlen(left)];//截取出操作符
        //这是在原字符串里面取出来，buf里面的这个位置被改成\0了

        _x = atoi(left);
        _y = atoi(right);
        _ops = mid;
        *status=true;
    }
    // 序列化 （入参应该是空的）
    void serialize(std::string& out)
    {
#ifdef MYPROTOCOL
        // x + y
        out.clear(); // 序列化的入参是空的
        out+= std::to_string(_x);
        out+= SPACE;
        out+= _ops;//操作符不能用tostring，会被转成ascii
        out+= SPACE;
        out+= std::to_string(_y);
        // 不用添加分隔符（这是encode要干的事情）
#else
        //使用jsoncpp的代码
        Json::Value root;
        root["x"] = _x;
        root["y"] = _y;
        root["op"] = _ops;

        Json::FastWriter fw; // 这个是写成一行，对于计算机来说处理的负担小
        // Json::StyledWriter fw; // 这个会进行格式化，更好看（但是内容没差距）
        out = fw.write(root);
#endif
    }
    // 反序列化
    bool deserialize(const std::string &in)
    {
#ifdef MYPROTOCOL
        // x + y 需要取出x，y和操作符
        size_t space1 = in.find(SPACE); //第一个空格
        if(space1 == std::string::npos)
        {
            return false;
        }
        size_t space2 = in.rfind(SPACE); //第二个空格
        if(space2 == std::string::npos)
        {
            return false;
        }
        // 两个空格都存在，开始取数据
        std::string dataX = in.substr(0,space1);
        std::string dataY = in.substr(space2+SPACE_LEN);//默认取到结尾
        std::string op = in.substr(space1+SPACE_LEN,space2 -(space1+SPACE_LEN));
        if(op.size()!=1)
        {
            return false;//操作符长度有问题
        }

        //没问题了，转内部成员
        _x = atoi(dataX.c_str());
        _y = atoi(dataY.c_str());
        _ops = op[0];
        return true;
#else
        //json
        Json::Value root;
        Json::Reader rd;
        rd.parse(in, root);
        _x = root["x"].asInt();
        _y = root["y"].asInt();
        _ops = root["op"].asInt();
        return true;
#endif
    }
    // debug打印成员变量
    void debug()
    {
        std::cout << "####Req debug####" << std::endl;
        std::cout << "_x   " << _x << std::endl;
        std::cout << "_y   " << _y << std::endl;
        std::cout << "_ops " << _ops << std::endl;
        std::cout << "#################" << std::endl;
    }


    // 删除输入中的空格
    void rmSpace(std::string& in)
    {
        std::string tmp;
        for(auto e:in)
        {
            if(e!=' ')
            {
                tmp+=e;
            }
        }
        in = tmp;
    }

    int _x;
    int _y;
    char _ops;
};


class Response
{
public:
    Response(int code=0,int result=0)
        :_exitCode(code),_result(result)
    {}

    // 入参是空的
    void serialize(std::string& out)
    {
#ifdef MYPROTOCOL
        // code ret
        out.clear();
        out+= std::to_string(_exitCode);
        out+= SPACE;
        out+= std::to_string(_result);
        out+= CRLF;
#else
        //使用jsoncpp的代码
        Json::Value root;
        root["exitCode"] = _exitCode;
        root["result"] = _result;

        Json::FastWriter fw; // 这个是写成一行，对于计算机来说处理的负担小
        // Json::StyledWriter fw; // 这个会进行格式化，更好看（但是内容没差距）
        out = fw.write(root);
#endif
    }

    // 反序列化
    bool deserialize(const std::string &in)
    {
#ifdef MYPROTOCOL
        // 只有一个空格
        size_t space = in.find(SPACE);
        if(space == std::string::npos)
        {
            return false;
        }

        std::string dataCode = in.substr(0,space);
        std::string dataRes = in.substr(space+SPACE_LEN);
        _exitCode = atoi(dataCode.c_str());
        _result = atoi(dataRes.c_str());
        return true;
#else
        //json读取
        Json::Value root;
        Json::Reader rd;
        rd.parse(in, root);
        _exitCode = root["exitCode"].asInt();
        _result = root["result"].asInt();
        return true;
#endif
    }

    void debug()
    {
        std::cout << "####Resp debug####" << std::endl;
        std::cout << "_exitCode " << _exitCode << std::endl;
        std::cout << "_result      " << _result << std::endl;
        std::cout << "##################" << std::endl;
    }


    int _exitCode; //计算服务的退出码
    int _result;  // 结果
};  