#include <iostream>
#include <string>
#include <arpa/inet.h>

void usage(char *argv[])
{
    std::cout << "Usage: \n"
              << argv[0] << " ip -s gmask" << std::endl
              << argv[0] << " ip -i 25(子网掩码的数字形式)" << std::endl;
    ;
}

std::string num_to_gmask(size_t maskBits)
{
    unsigned int maskPart1 = (maskBits >= 8) ? 255 : (255U << (8 - maskBits));
    unsigned int maskPart2 = (maskBits >= 16) ? 255 : (255U << (16 - maskBits)) & 255U;
    unsigned int maskPart3 = (maskBits >= 24) ? 255 : (255U << (24 - maskBits)) & 255U;
    unsigned int maskPart4 = (maskBits >= 32) ? 255 : (255U << (32 - maskBits)) & 255U;

    // 将子网掩码的四个部分拼接成一个字符串
    char subnetMaskStr[16]; // 子网掩码字符串最大长度为15

    // 使用sprintf将四个部分拼接成子网掩码字符串
    sprintf(subnetMaskStr, "%u.%u.%u.%u", maskPart1, maskPart2, maskPart3, maskPart4);
    return std::string(subnetMaskStr);
}

void ip_and_gmask_test()
{
    unsigned int ipAddress = 172 << 24 | 16 << 16 | 0 << 8 | 2;   // 将 IP 地址转换为 32 位无符号整数
    unsigned int subnetMask = 255 << 24 | 255 << 16 | 0 << 8 | 0; // 将子网掩码转换为 32 位无符号整数

    unsigned int networkAddress = ipAddress & subnetMask; // 按位与

    std::cout << "IP地址:  172.16.0.2" << std::endl;
    std::cout << "子网掩码: 255.255.0.0" << std::endl;
    std::cout << "网络地址: " << (networkAddress >> 24) << "." << ((networkAddress >> 16) & 255) << "." << ((networkAddress >> 8) & 255) << "." << (networkAddress & 255) << std::endl;
}

void ip_and_gmask_linux(const char *ip, const char *gmask)
{
    // 将点分十进制的IP地址和子网掩码转换为无符号整数
    unsigned int ipAddress = inet_addr(ip);
    unsigned int subnetMask = inet_addr(gmask);

    // 进行按位与操作得到网络地址
    unsigned int networkAddress = ipAddress & subnetMask;

    // 将网络地址转换回点分十进制表示法并输出结果
    struct in_addr addr;
    addr.s_addr = networkAddress;
    std::cout << "IP地址:   "
              << ip << std::endl;
    std::cout << "子网掩码: "
              << gmask << std::endl;
    std::cout << "网络地址: " << inet_ntoa(addr) << std::endl;
    return;
}

int main(int argc, char *argv[])
{
    // ip_and_gmask_test();
    if (argc != 4)
    {
        usage(argv);
        return 1;
    }
    std::string arg(argv[2]);
    if (arg == "-s")
    {
        ip_and_gmask_linux(argv[1], argv[3]);
    }
    else if (arg == "-i")
    {
        ip_and_gmask_linux(argv[1], num_to_gmask(std::atoi(argv[3])).c_str());
    }
    else
    {
        usage(argv);
        return 1;
    }

    return 0;
}
