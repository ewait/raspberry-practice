#include <iostream>
#include <future>
#include <functional>
#include <thread>
#include <type_traits> // result_of
#include <sys/unistd.h> // sleep

int add(int a, int b)
{
    std::cout << "Add  Thread " << std::this_thread::get_id() << " | Sleeping before add..." << std::endl;
    sleep(4);
    throw std::runtime_error("An error occurred");
    return a + b;
}

// 声明函数指针类型
void test_decltype()
{
    std::result_of<decltype(add) &(int, int)>::type result = add(3, 5);
    std::cout << "Result: " << result << std::endl;
}

void future_get_func_shared(std::shared_future<int>& fu)
{
    int result = fu.get();  // 等待异步任务完成并获取结果
    std::cout << "Func Thread " << std::this_thread::get_id() << " | Result: " << result << std::endl;
    sleep(2);
}

void future_get_func(std::future<int>& fu)
{
    int result = fu.get();  // 等待异步任务完成并获取结果
    std::cout << "Func Thread " << std::this_thread::get_id() << " | Result: " << result << std::endl;
    sleep(2);
}


int main() 
{
    try
    {
        std::cout << "Main Thread " << std::this_thread::get_id() << " | Start" << std::endl;
        std::future<int> futureResult = std::async(std::launch::deferred, add, 3, 5); // 不会创建新线程
        //std::future<int> futureResult = std::async(std::launch::async, add, 3, 5); // 创建新线程
        sleep(3); 
        std::cout << "Main Thread " << std::this_thread::get_id() << " | Waiting for result..." << std::endl;
        
        // 尝试测试多线程get会发生什么
        // auto sfu = futureResult.share();

        std::thread t1(add,3,4);
        t1.detach();

        // std::thread t1(future_get_func, std::ref(futureResult)); // 开个线程来get
        // t1.detach(); // 直接分离线程
        // sleep(1);

        int result = futureResult.get();  // 等待异步任务完成并获取结果
        std::cout << "Main Thread " << std::this_thread::get_id() << " | Result: " << result << std::endl;
        sleep(3);
    }
    catch(...)
    {
        std::cerr <<  "exception occur\n";
    }
    
    return 0;
}
