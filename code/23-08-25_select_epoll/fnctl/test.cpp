#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

void SetNoBlock(int fd) 
{ 
    int fl = fcntl(fd, F_GETFL);  // 先获取文件已有状态
    if (fl < 0) { 
       perror("fcntl error");
       return;
    }
    // 在已有状态的基础上，设置O_NONBLOCK非阻塞
    fcntl(fd, F_SETFL, fl | O_NONBLOCK);  
}


int main()
{
    SetNoBlock(stdin->_fileno);
    char buf[1024];
    while(true)
    {
        ssize_t read_size = read(stdin->_fileno, buf, sizeof(buf) - 1);
        if(read_size < 0)
        {
            perror("read err");
            sleep(2);
            continue;
        }
        printf("input:%s\n", buf);
        buf[0] = '\0';
    }
    return 0;
}