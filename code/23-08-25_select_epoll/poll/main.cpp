#include <iostream>
#include <poll.h>
#include "Sock.hpp"

#define NUM 1024
struct pollfd fdsArray[NUM]; // 保存历史上所有的合法fd

#define DFL_FD -1

using namespace std;

static void ShowArray(struct pollfd arr[], int num)
{
    cout << "当前合法sock list# ";
    for (int i = 0; i < num; i++)
    {
        if (arr[i].fd == DFL_FD)
            continue;
        else
            cout << arr[i].fd << " ";
    }
    cout << endl;
}

static void CmdUsage(std::string process)
{
    cerr << "\nUsage: " << process << " port\n"
         << endl;
}
// readfds: 现在包含就是已经就绪的sock
static void HandlerEvent(int listensock)
{
    for (int i = 0; i < NUM; i++)
    {
        if (fdsArray[i].fd == DFL_FD)
            continue;
        if (i == 0 && fdsArray[i].fd == listensock)
        {
            // 我们是如何得知哪些fd，上面的事件就绪呢？
            if (fdsArray[i].revents & POLLIN)
            {
                // 具有了一个新链接
                cout << "已经有一个新链接到来了，需要进行获取(读取/拷贝)了" << endl;
                string clientip;
                uint16_t clientport = 0;
                int sock = Sock::Accept(listensock, &clientip, &clientport); // 不会阻塞
                if (sock < 0)
                    return;
                cout << "获取新连接成功: " << clientip << ":" << clientport << " | sock: " << sock << endl;

                int j = 0;
                for (j = 0; j < NUM; j++)
                {
                    if (fdsArray[j].fd == DFL_FD)
                        break;
                }
                // 通过for循环找到当前位置是默认的结构体下标
                if (j == NUM) // 如果达到上限才break
                {
                    cerr << "我的服务器已经到了最大的上限了，无法在承载更多同时保持的连接了" << endl;
                    close(sock);
                }
                else
                {
                    fdsArray[j].fd = sock; // 将sock添加到数组中
                    fdsArray[j].events = POLLIN;
                    fdsArray[j].revents = 0;
                    ShowArray(fdsArray, NUM);
                }
            }
        } // end if (i == 0 && fdsArray[i] == listensock)
        else
        {
            // 处理普通sock的IO事件！
            if(fdsArray[i].revents & POLLIN)
            {
                // 一定是一个合法的普通的IO类sock就绪了
                // read/recv读取即可
                // TODO bug
                char buffer[1024];
                ssize_t s = recv(fdsArray[i].fd, buffer, sizeof(buffer), 0); // 不会阻塞
                if(s > 0)
                {
                    buffer[s] = 0;
                    cout << "client[" << fdsArray[i].fd << "]# " << buffer << endl; 
                    
                }
                else if(s == 0)
                {
                    cout << "client[" << fdsArray[i].fd << "] quit, server close " << fdsArray[i].fd << endl;
                    close(fdsArray[i].fd);
                    fdsArray[i].fd = DFL_FD; // 去除对该文件描述符的select事件监听
                    fdsArray[i].events = 0;
                    fdsArray[i].revents = 0;
                    ShowArray(fdsArray, NUM);
                }
                else
                {
                    cout << "client[" << fdsArray[i].fd << "] quit, server error " << fdsArray[i].fd << endl;
                    close(fdsArray[i].fd);
                    fdsArray[i].fd = DFL_FD; // 去除对该文件描述符的select事件监听
                    fdsArray[i].events = 0;
                    fdsArray[i].revents = 0;
                    ShowArray(fdsArray, NUM);
                }
            }
        }
    }
}

// ./SelectServer 8080
// 只关心读事件
int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        CmdUsage(argv[0]);
        exit(1);
    }
    int listensock = Sock::SocketInit();
    Sock::Bind(listensock, atoi(argv[1]));
    Sock::Listen(listensock);

    for (int i = 0; i < NUM; i++)
    {
        fdsArray[i].fd = DFL_FD;
        fdsArray[i].events = 0;
        fdsArray[i].revents = 0;
    }
    fdsArray[0].fd = listensock;
    fdsArray[0].events = POLLIN;
    int timeout = -1;
    while (true)
    {
        int n = poll(fdsArray, NUM, timeout);
        switch (n)
        {
        case 0:
            cout << "time out ... : " << (unsigned long)time(nullptr) << endl;
            break;
        case -1:
            cerr << errno << " : " << strerror(errno) << endl;
            break;
        default:
            HandlerEvent(listensock);
            break;
        }
    }

    return 0;
}
