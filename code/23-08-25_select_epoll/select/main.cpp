#include <iostream>
#include "Sock.hpp"
#include <sys/select.h>
#include <sys/unistd.h>
using namespace std;

int fdsArray[sizeof(fd_set) * 8] = {0}; // 保存历史上所有的合法fd
int fdsArraySz = sizeof(fdsArray) / sizeof(fdsArray[0]);
#define DFL_FD -1 // 数组中默认值

// 使用static保证这个函数只有main里面存在
static void CmdUsage(std::string process)
{
    cerr << "\nUsage: " << process << " port\n"
         << endl;
}

/// @brief 打印数组中的文件描述符
static void ShowArray(int arr[], int num)
{
    cout << "当前合法sock list: ";
    for (int i = 0; i < num; i++)
    {
        if (arr[i] == DFL_FD)
            continue;
        else
            cout << arr[i] << " ";
    }
    cout << endl;
}

/// @brief 用来处理新连接和为已有链接提供服务的函数
/// @param listensock 监听描述符
/// @param readfds 已经就绪的文件描述符位图
static void HandlerEvent(int listensock, fd_set &readfds)
{
    for (int i = 0; i < fdsArraySz; i++)
    {
        if (fdsArray[i] == DFL_FD)
            continue;
        if (i == 0 && fdsArray[i] == listensock)
        {
            // 判断listensocket有没有事件监听
            if (!FD_ISSET(listensock, &readfds)){
                cerr << "listensocket not set in readfds" << endl;
                continue;
            }
            // 具有了一个新链接
            cout << "get new connection" << endl;
            string clientip;
            uint16_t clientport = 0;
            int sock = Sock::Accept(listensock, &clientip, &clientport); // 不会阻塞
            if (sock < 0)
                return; // 出错了，直接返回
            // 成功获取新连接
            cout << "new conn:" << clientip << ":" << clientport << " | sock: " << sock << endl;

            // 这里我们不能直接对这个socket进行独写，因为新链接来了并不代表新数据一并过来了
            // 所以需要将新的文件描述符利用全局数组，交付给select
            // select 帮我们监看socket上的读事件是否就绪
            int i = 0;
            for (i = 0; i < fdsArraySz; i++)
            {
                if (fdsArray[i] == DFL_FD)
                    break;
            }
            // 达到上限了
            if (i == fdsArraySz)
            {
                cerr << "reach the maximum number of connections" << endl;
                close(sock);
            }
            else // 没有达到
            {
                fdsArray[i] = sock; // 新的链接，插入到数组中，下次遍历就会添加到select监看中
                ShowArray(fdsArray, fdsArraySz);
            }
        } // end if (i == 0 && fdsArray[i] == listensock)
        else
        {
            // 处理普通sock的IO事件
            if (FD_ISSET(fdsArray[i], &readfds))
            {
                // read、recv读取即可
                char buffer[1024];
                ssize_t s = recv(fdsArray[i], buffer, sizeof(buffer), 0); // 不会阻塞
                if (s > 0)
                {
                    buffer[s] = 0;
                    cout << "client[" << fdsArray[i] << "]# " << buffer << endl;
                }
                else if (s == 0) // 对端关闭
                {
                    cout << "client[" << fdsArray[i] << "] quit, server close " << fdsArray[i] << endl;
                    close(fdsArray[i]);
                    fdsArray[i] = DFL_FD; // 去除对该文件描述符的select事件监听
                    ShowArray(fdsArray, fdsArraySz);
                }
                else // 异常了
                {
                    cout << "client[" << fdsArray[i] << "] error, server close " << fdsArray[i] << endl;
                    close(fdsArray[i]);
                    fdsArray[i] = DFL_FD; // 去除对该文件描述符的select事件监听
                    ShowArray(fdsArray, fdsArraySz);
                }
            }
        }
    }
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        CmdUsage(argv[0]);
        exit(1);
    }
    // 初始化socket，获取socket fd并绑定端口
    int listensock = Sock::SocketInit();
    Sock::Bind(listensock, atoi(argv[1]));
    Sock::Listen(listensock); // 开始监听

    // 将数组里面的文件描述符都初始化为默认值，并将第一个下标设置为listensocket
    for (int i = 0; i < fdsArraySz; i++)
    {
        fdsArray[i] = DFL_FD;
    }
    fdsArray[0] = listensock;
    // 开始监听
    fd_set readfds;
    while (true)
    {
        int maxFd = DFL_FD;
        FD_ZERO(&readfds);               // 清空位图
        struct timeval timeout = {5, 0}; // 设置超时时间为5秒
        // 遍历全局数组，将有效的fd都添加进去，并更新maxfd
        for (int i = 0; i < fdsArraySz; i++)
        {
            // 1. 过滤不合法的fd
            if (fdsArray[i] == DFL_FD)
                continue;
            // 2. 添加所有的合法的fd到readfds中，方便select统一进行就绪监听
            FD_SET(fdsArray[i], &readfds);
            if (maxFd < fdsArray[i])
            {
                maxFd = fdsArray[i]; // 3. 更新出fd最大值
            }
        }
        // 调用select开始监听
        int sret = select(maxFd + 1, &readfds, nullptr, nullptr, &timeout);
        switch (sret)
        {
        case 0: // 等待超时
            cout << "time out ... : " << (unsigned long)time(nullptr) << endl;
            break;
        case -1: // 等待失败
            cerr << errno << " : " << strerror(errno) << endl;
            break;
        default:
            // 等待成功
            cout << "wait success: " << sret << endl;
            HandlerEvent(listensock, readfds);
            break;
        }
    }

    return 0;
}