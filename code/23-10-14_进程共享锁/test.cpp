#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int main()
{
    // 共享内存的名字必须以/开头
    int shared_mem_fd = shm_open("/my_shared_memory", O_CREAT | O_RDWR, 0666);
    // 创建了一个mutex大小的共享内存(这个函数的作用是将fd文件给截断/扩展为第二个参数的大小)
    ftruncate(shared_mem_fd, sizeof(pthread_mutex_t));
    // 用mmap挂载到本地
    // addr：指定映射的虚拟地址，通常设置为NULL，让系统自动分配。
    // length：指定映射的长度，以字节为单位。
    // prot：指定映射区域的保护权限，可选值为PROT_READ（可读权限）、PROT_WRITE（可写权限）、PROT_EXEC（可执行权限）以及它们的组合。
    // flags：指定映射的类型和其他标志位，常见的标志位有MAP_SHARED（共享映射）、MAP_PRIVATE（私有映射）、MAP_ANONYMOUS（匿名映射）等。
    // fd：如果要映射文件，则为文件描述符；如果映射的是匿名内存区域，则传入-1。
    // offset：从文件开始处的偏移量，通常设置为0。
    //
    // 返回值：成功时，返回映射区域的起始地址指针；
    //        失败时，返回MAP_FAILED，并设置errno来指示错误类型。
    void *shared_mem_ptr = mmap(NULL, sizeof(pthread_mutex_t), PROT_READ | PROT_WRITE, MAP_SHARED, shared_mem_fd, 0);

    pthread_mutexattr_t attr;
    pthread_mutexattr_init(&attr);
    pthread_mutexattr_setpshared(&attr, PTHREAD_PROCESS_SHARED); // 设置锁的属性为共享锁
    // 如果删除上面这一行，那么当下这个锁就是父进程的私有锁，不具有公有属性
    // 即便我们使用共享内存将锁映射到了父子进程的页表中
    // 观察到的现象是，即便父进程已经释放锁了，但是子进程依旧是在阻塞状态中
    // 个人猜测：因为这个锁是父进程独有的，所以子进程在调用lock函数访问mutex的时候，实际上是将mutex进行了一次写时拷贝；
    //          拷贝的时候，这个锁是被占用的，拷贝过去之后也是一个被占用中的锁。
    //          但实际上压根没有进程在占用这个被子进程拷贝出去的独立的锁，父进程的解锁操作也不会在进程间同步，这就是一种死锁。

    pthread_mutex_t *mutex = (pthread_mutex_t *)shared_mem_ptr;
    pthread_mutex_init(mutex, &attr); // 指定使用共享内存的地址来初始化锁

    pid_t pid = fork();
    if (pid < 0)
    {
        fprintf(stderr, "Fork failed.\n");
        return 1;
    }
    else if (pid == 0)
    {
        // Child process
        sleep(1); // 子进程先休眠1秒，等待夫进程获取锁
        printf("Child trying to acquire the mutex... %p\n", mutex);
        pthread_mutex_lock(mutex); // 子进程获取锁，这时候父进程在休眠，无法获取
        printf("Child acquired the mutex.\n");
        // Do some work...
        sleep(2);
        pthread_mutex_unlock(mutex);
        printf("Child released the mutex.\n");
    }
    else
    {
        // Parent process
        printf("Parent trying to acquire the mutex... %p\n", mutex);
        pthread_mutex_lock(mutex);
        printf("Parent acquired the mutex.\n");
        // Do some work...
        sleep(2);
        pthread_mutex_unlock(mutex); // 父进程释放锁后，观察到的情况是子进程成功获取锁
        printf("Parent released the mutex.\n");
        wait(NULL); // 等待子进程执行完毕

        pthread_mutexattr_destroy(&attr); // 父进程来销毁相关资源
        pthread_mutex_destroy(mutex);
        munmap(shared_mem_ptr, sizeof(pthread_mutex_t));
        shm_unlink("/my_shared_memory");
    }

    return 0;
}