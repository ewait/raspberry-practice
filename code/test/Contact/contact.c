#define _CRT_SECURE_NO_WARNINGS 1
//这是一个测试
#include"contact.h"

////清屏函数
//void clear()
//{
	//system("pause");
	//system("cls");
//}
//容量检查
void CheckCapacity(Contact* pc)
{
	if (pc->sz == DEFAULT_SZ)
	{
		PeoInfo* tmp = (PeoInfo*)realloc(pc->data, (pc->capacity + 5) * sizeof(PeoInfo));
		if (tmp != NULL)
		{
			pc->data = tmp;
			pc->capacity += 5;
			printf("扩容成功\n");
		}
		else
		{
			printf("CheckCapacity()::%s\n", strerror(errno));
			return;
		}
	}
}
//读取文件中的保存数据
void LoadContact(Contact* pc) 
{
	//读取文件
	FILE* pf = fopen("contact.txt", "rb");
	if (pf == NULL)
	{
		printf("LoadContact::%s\n", strerror(errno));
		return;
	}

	PeoInfo buf = { 0 };
	while (fread(&buf, sizeof(PeoInfo), 1, pf))
	{
		CheckCapacity(pc);

		pc->data[pc->sz] = buf;
		pc->sz++;
	}


	fclose(pf);
	pf = NULL;
}

//初始化
void Initial(Contact* pc)
{
	assert(pc);
	pc->sz = 0;
	PeoInfo* tmp = (PeoInfo*)calloc(DEFAULT_SZ, sizeof(PeoInfo));
	if (tmp != NULL)
	{
		pc->data = tmp;
	}
	else
	{
		printf("Initial()::%s\n", strerror(errno));
		return;
	}
	pc->capacity = DEFAULT_SZ;


	LoadContact(pc);
}

//释放通讯录
void DestroyContact(Contact* pc)
{
	assert(pc);
	free(pc->data);
	pc->data = NULL;
	pc->sz = 0;
	pc->capacity = 0;
}

//增加联系人
void AddContact(Contact* pc)
{
	assert(pc);
	
	CheckCapacity(pc);//查看是否需要扩容

	printf("输入姓名>");
	scanf("%s", pc->data[pc->sz].name);
	printf("输入年龄>");
	scanf("%d", &(pc->data[pc->sz].age));
	printf("输入性别>");
	scanf("%s", pc->data[pc->sz].sex);
	printf("输入电话>");
	scanf("%s", pc->data[pc->sz].pho);
	printf("输入地址>");
	scanf("%s", pc->data[pc->sz].adr);

	pc->sz++;
	printf("增加联系人成功\n");
}

//打印联系人
void ShowContact(Contact* pc)
{
	assert(pc);
	if (pc->sz == 0)
	{
		printf("通讯录为空\n");
		return;
	}
	printf("%-10s\t%-4s\t%-5s\t%-15s\t%-20s\t\n", "姓名", "年龄", "性别", "电话", "地址");
	int i = 0;
	for (i = 0; i < pc->sz; i++)
	{
		printf("%-10s\t%-4d\t%-5s\t%-15s\t%-20s\t\n", 
			pc->data[i].name,pc->data[i].age,pc->data[i].sex,pc->data[i].pho,pc->data[i].adr);
	}

}

//查找联系人
int FindContact(Contact* pc)
{
	assert(pc);
	char name[MAX_NAME] = { 0 };
	scanf("%s", name);
	int i = 0;
	for (i = 0; i < pc->sz; i++)
	{
		if (strcmp(name, pc->data[i].name) == 0)
		{
			return i;
		}
	}
	return -1;
}
//查找到后需要打印
void SearchContact(Contact* pc)
{
	assert(pc);
	printf("输入需要查找的联系人姓名>");
	int i = FindContact(pc);
	if (i != -1)
	{
		printf("找到联系人如下:\n");
		printf("%-10s\t%-4s\t%-5s\t%-15s\t%-20s\t\n", "姓名", "年龄", "性别", "电话", "地址");
		printf("%-10s\t%-4d\t%-5s\t%-15s\t%-20s\t\n",
			pc->data[i].name, pc->data[i].age, pc->data[i].sex, pc->data[i].pho, pc->data[i].adr);
	}
	else
	{
		printf("该联系人不存在\n");
	}
}

//删除联系人
void DelContact(Contact* pc)
{
	assert(pc);
	if (pc->sz == 0)
	{
		printf("通讯录为空\n");
	}
	printf("输入需要删除的联系人姓名>");
	int i = FindContact(pc);
	if (i == -1)
	{
		printf("该联系人不存在\n");
	}
	else
	{
		int j = 0;
		for (j = i; j < pc->sz - 1; j++)
		{
			pc->data[j] = pc->data[j + 1];
		}
		pc->sz--;
		printf("删除指定联系人成功\n");
	}

}

//更改联系人
void ModifyContact(Contact* pc) 
{
	assert(pc);
	if(pc->sz == 0)
	{
		printf("通讯录为空\n");
	}
	printf("输入需要更改的联系人姓名>");
	int i = FindContact(pc);
	if (i == -1)
	{
		printf("该联系人不存在\n");
	}
	else
	{
		printf("已找到该联系人\n");
		printf("输入更改后的姓名>");
		scanf("%s", pc->data[i].name);
		printf("输入更改后的年龄>");
		scanf("%d", &(pc->data[i].age));
		printf("输入更改后的性别>");
		scanf("%s", pc->data[i].sex);
		printf("输入更改后的电话>");
		scanf("%s", pc->data[i].pho);
		printf("输入更改后的地址>");
		scanf("%s", pc->data[i].adr);
		
		printf("更改指定联系人成功\n");
	}
}

//按名字排序联系人
int cmp_stu_by_name(const void* e1, const void* e2)
{  
	return (strcmp(((PeoInfo*)e1)->name, ((PeoInfo*)e2)->name));
}

void SortContact(Contact* pc)
{
	assert(pc);
	if (pc->sz == 0)
	{
		printf("通讯录为空\n");
	}
	else
	{
		qsort(pc->data, pc->sz, sizeof(pc->data[0]), cmp_stu_by_name);
		printf("排序成功\n");
		ShowContact(pc);
	}
}

void SaveContact(Contact* pc)
{
	//读取文件
	FILE* pf = fopen("contact.txt", "wb");
	if (pf == NULL)
	{
		printf("SaveContact::%s\n", strerror(errno));
		return;
	}
	//写文件
	int i = 0;
	for (i = 0; i <(pc->sz); i++)
	{
		fwrite(pc->data + i, sizeof(PeoInfo), 1, pf);
	}

	fclose(pf);
	pf = NULL;
	printf("通讯录保存成功\n");
}
