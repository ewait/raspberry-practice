//#pragma once
//这是一个测试
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <errno.h>

#define MAX 100
#define DEFAULT_SZ 3

#define MAX_NAME 20
#define MAX_SEX 5
#define MAX_PHO 15
#define MAX_ADR 30

typedef struct PeoInfo
{
	char name[MAX_NAME];
	int age;
	char sex[MAX_SEX];//性别
	char pho[MAX_PHO];//电话
	char adr[MAX_ADR];//地址
}PeoInfo;

//通讯录结构体
typedef struct Contact
{
	//PeoInfo data[MAX];
	PeoInfo* data;//存放数据
	int sz;//有效数据个数
	int capacity;//容量
}Contact;

//清屏函数-树莓派不支持
//void clear();

//初始化文件中的通讯录
void LoadContact(Contact* pc);
//初始化结构体
void Initial(Contact* pc);

//增加联系人
void AddContact(Contact* pc);

//打印联系人
void ShowContact(Contact* pc);

//查找联系人
int FindContact(Contact* pc);
void SearchContact(Contact* pc);

//删除联系人
void DelContact(Contact* pc);

//更改联系人
void ModifyContact(Contact* pc);

//比较名字的函数
int cmp_stu_by_name(const void* e1, const void* e2);
//按照名字排序联系人
void SortContact(Contact* pc);

//保存通讯录到文件中
void SaveContact(Contact* pc);

//释放空间
void DestroyContact(Contact* pc);

