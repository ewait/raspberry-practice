﻿#define _CRT_SECURE_NO_WARNINGS 1

#include"Seqlist.h"

void CheckCapacity(SeqList* sql)
{
	assert(sql);

	if (sql->size < sql->capacity)
		return;
	else
	{
		size_t newcapacity = 2 * (sql->capacity);
		SLDataType* tmp = (SLDataType*)realloc(sql->a, newcapacity*sizeof(SLDataType));
		if (tmp == NULL)
		{
			printf("realloc failed\n");
			exit(0);
		}
		else
		{
			sql->a = tmp;
			sql->capacity = newcapacity;
		}
	}
	return;
}


void SQLinst(SeqList* sql)
{
	assert(sql);

	sql->a = (SLDataType*)calloc(CAPA, sizeof(SLDataType));
	sql->capacity = CAPA;
	sql->size = 0;

	return;
}

void SQLdestory(SeqList* sql)
{
	assert(sql);

	free(sql->a);
	sql->a = NULL;
	sql->capacity = 0;
	sql->size = 0;

	return; 
}

void SQLprint(SeqList* sql)
{
	assert(sql);

	for (int i = 0; i < (sql->size); i++)
	{
		printf("%d ", sql->a[i]);
	}
	printf("\n");

	return;
}


void SQLpushback(SeqList* sql, size_t x)
{
	assert(sql);

	CheckCapacity(sql);

	sql->a[sql->size] = x;
	sql->size++;

	return;
}

void SQLpopback(SeqList* sql)
{
	assert(sql);

	//sql->a[sql->size] = 0;
	sql->size--;

	return;
}


void SQLpushfront(SeqList* sql, size_t x)
{
	assert(sql);
	CheckCapacity(sql);

	int i = sql->size;
	while (i >= 0)
	{
		sql->a[i] = sql->a[i - 1];
		i--;
	}
	sql->a[0] = x;
	sql->size++;

	return;
}

void SQLpopfront(SeqList* sql)
{
	assert(sql);

	int i = 0;
	while (i < (int)sql->size)
	{
		sql->a[i] = sql->a[i + 1];
		i++;
	}

	sql->size--;

	return;
}

void SQLinsert(SeqList* sql, size_t pos, size_t x)
{
	assert(sql);
	if (pos >= (int)sql->size)
	{//温和的判断，assert太过暴力，在OJ里容易出错
		printf("input err\n");
		return;
	}
	CheckCapacity(sql);
	int i = sql->size;
	while (i > (int)pos)
	{
		sql->a[i] = sql->a[i - 1];
		i--;
	}

	sql->a[pos]=x;
	sql->size++;

	return;
}

void SQLerase(SeqList* sql, size_t pos)
{
	assert(sql);
	if (pos >= (int)sql->size)
	{//温和的判断，assert太过暴力，在OJ里容易出错
		printf("input err\n");
		return;
	}

	int i = pos;
	while (i < (int)sql->size-1)
	{
		sql->a[i] = sql->a[i + 1];
		i++;
	}

	sql->size--;

	return;
}

int SQLfind(SeqList* sql, size_t x)
{
	assert(sql);

	for (int i = 0; i < sql->size; i++)
	{
		if (sql->a[i] == x)
		{
			return i;//返回下标
		}
	}
	printf("find err\n");
	return -1;
}

void SQLmodify(SeqList* sql, size_t pos, size_t x)
{
	assert(sql);

	sql->a[pos] = x;
	
	return;
}