#include <iostream>
using namespace std;

class mytest
{
public:
    mytest() = default;
    mytest(int a):_a(0){}

    void set_int(int a)
    {
        cout << "set int to " << a << endl;
        _a = a;
    }

    void print(int a)
    {
        cout << "just a print" << a << endl;
    }

private:
    int _a = 0;
};


int main()
{
    mytest* ptr = nullptr;
    ptr->print(20);
    ptr->set_int(10);

    return 0;
}