﻿#define  _CRT_SECURE_NO_WARNINGS 1
//# pragma warning (disable:4819)
////offsetof - 宏
////getchar
#include <stdio.h>

//#define MAX(x, y) ((x)>(y)?(x):(y))
//
//int Max(int x, int y)
//{
//	return x > y ? x : y;
//}
//
//int main()
//{
//#undef MAX
//	int m = MAX(2, 3);
//	printf("%d\n", m);
//
//	return 0;
//}


//#define M 0
//int main()
//{
	//int i = 0;
	//int n = 10;
	//for (i = 0; i < 10; i++)
	//{
//#if M
		//printf("%d\n", i);
//#endif
		////其他代码
	//}

	//return 0;
//}

//
//
//
//#define M 150
//
//int main()
//{
//#if M<100
//	printf("less\n");
//#elif M==100
//	printf("==\n");
//#elif M>100&&M<200
//	printf("more\n");
//#else
//	printf("hehe\n");
//#endif
//
//	return 0;
//}


//#define M 0
//
//int main()
//{
////#if defined(M)
////	printf("hehe\n");
////#endif
////
////#ifdef M
////	printf("haha\n");
////#endif
//
//
//#if !defined(M)
//	printf("hehe\n");
//#endif
//
//#ifndef M
//	printf("haha\n");
//#endif
//
//	return 0;
//}

//#include "test.h"
//#include "test.h"
//#include "test.h"
//#include "test.h"


//#include <stdio.h>
//只在库目录下查找

//#include "stdio.h"
//1.当前工程目录查找
//2.库目录下查找

#include "test.h"
#include "test.h"
int main()
{
	printf("hehe\n");
	printf("%d\n",Add(3,5));
	return 0;
}

//
//////模拟实现atoi
//#include <stdlib.h>
//#include <assert.h>
////
//////这种方法想法过于简单！err
//////int my_atoi(char* str)
//////{
//////	int n = 0;
//////	assert(str);
//////
//////	while (*str)
//////	{
//////		n = n * 10 + (*str - '0');
//////		str++;
//////	}
//////	return n;
//////}
////
//////
////1. 空指针
////2. 空字符串
////3. 空白字符
////4. +-
////5. 非数字字符
////6. 超大数字
////
////
//#include <ctype.h>
////
//enum State
//{
//	INVALID,//非法
//	VALID   //合法
//};
//
//enum State status = INVALID;
//
//int my_atoi(const char* str)
//{
//	assert(str);
//	//空字符串
//	if (*str == '\0')
//		return 0;
//
//	//空白字符（跳过）
//	while (isspace(*str))
//	{
//		str++;
//	}
//	int flag = 1;
//	
//	//+-
//	if (*str == '+')
//	{
//		str++;
//		flag = 1;
//	}
//	else if (*str == '-')
//	{
//		str++;
//		flag = -1;
//	}
//
//	long long n = 0;
//	while (isdigit(*str))
//	{
//		n = n * 10 + flag*(*str - '0');
//		//越界的值
//		if (n > INT_MAX || n < INT_MIN)
//		{
//			return 0;
//		}
//		str++;
//	}
//	if (*str == '\0')
//	{
//		//合法返回
//		status = VALID;
//		return (int)n;
//	}
//
//	return (int)n;
//}
//
//
//int main()
//{
//	int ret = my_atoi("-134");//"0"
//	if (status == VALID)
//		printf("%d\n", ret);
//	else
//		printf("非法返回\n");
//
//	return 0;
//}



//
//void test()
//{
//    exit(0);
//}
//
//int main()
//{
//    test();
//    long num = 0;
//    FILE* fp = NULL;
//    if ((fp = fopen("fname.dat", "r")) == NULL)
//    {
//        printf("Can’t open the file! ");
//        exit(0);
//    }
//    while (fgetc(fp) != EOF)
//    {
//        num++;
//    }
//    printf("num=%d\n", num);
//    fclose(fp);
//    return 0;
//}
//


//#define INT_PTR int*
//typedef int* int_ptr;
//
//int *a, b;
//int_ptr c, d;
//


//#define N 4
//
//#define Y(n) ((4+2)*n) /*这种定义在编程规范中是严格禁止的*/
//
//
//
//int main()
//{
//	printf("%d\n", 2 * (N + Y(5 + 1)));
//	return 0;
//}

//#define A 2+2
//#define B 3+3
//#define C A*B
//int main()
//{
//	printf("%d\n", C);
//	return 0;
//}

////写一个宏，可以将一个整数的二进制位的奇数位和偶数位交换。
//
//#include "test.h"
//
////10
////00000000000000000000000000001010 - 10
////01010101010101010101010101010101
////0x55555555
////10101010101010101010101010101010
////0xaaaaaaaa
////把10的所有偶数位保留下来，奇数位为0
////00000000000000000000000000001010 >>1
////00000000000000000000000000000101
////把10的所有奇数位保留下来，偶数位为0
////00000000000000000000000000000000 <<1
////00000000000000000000000000000000
//// 
////00000000000000000000000000000101 - 5
////
//
////
////1011 = 11
////0111
////1010 = 10
////0101 
////0001 = 1
////0010
////0111 = 7
//
//#define SWAP(x) (((x&0xaaaaaaaa)>>1) + ((x&0x55555555)<<1))
//
//int main()
//{
//	int a = 10;
//	printf("%d\n", SWAP(a));
//	return 0;
//}
 
 
 

//
//#include <stddef.h>
//
//struct S
//{
//	char c;
//	int a;
//	double d;
//};

////#define OFFSETOF(s_name, m_name)    (int)&(((struct S*)0)->c)
//
//#define OFFSETOF(s_name, m_name)    (int)&(((s_name*)0)->m_name)
//
//////假设首元素在0地址处，这时候地址转换为int就是偏移量
//int main()
//{
//	printf("%d\n", OFFSETOF(struct S, c));
//	printf("%d\n", OFFSETOF(struct S, a));
//	printf("%d\n", OFFSETOF(struct S, d));
//
//	return 0;
//}


