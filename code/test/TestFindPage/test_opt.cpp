#include <deque>
#include <vector>
#include <iostream>
#include <algorithm>
#include <map>

int main()
{
    std::vector<int> dq; // opt算法不需要用queue
    int n, num_page, num_lost_page = 0;
    std::cout << "[正在使用opt算法]" << std::endl;
    std::cout << "请输入物理页框块数> ";
    std::cin >> num_page;
    std::cout << "请输入页面走向个数> ";
    std::cin >> n;
    // 输入每个页的编号到数组中
    std::vector<int> num_array;
    for (int i = 0; i < n; i++)
    {
        int temp;
        std::cout << "请输入页面: ";
        std::cin >> temp;
        num_array.push_back(temp);
    }
    // 开始遍历处理
    int in;
    for (int i = 0; i < n; i++)
    {
        in = num_array[i];
        if (dq.size() < num_page) // 存在多余页框
        {
            bool is_in = false;
            for (auto &pos : dq) // 遍历队列
            {
                if (pos == in)
                {
                    is_in = true; // 当前页面已经在队列里面了。
                    std::cout << "页面 " << in << " 已在主存中" << std::endl;
                    break;
                }
            }
            if (!is_in) // 不存在此元素
            {
                num_lost_page++;
                dq.push_back(in);
            }
        }
        else // 不存在多余页框
        {
            bool is_in = false;
            for (auto &pos : dq) // 遍历队列
            {
                if (pos == in)
                {
                    is_in = true; // 当前页面已经在队列里面了。
                    std::cout << "页面 " << in << " 已在主存中" << std::endl;
                    break;
                }
            }
            if (!is_in) // 不存在此元素
            {
                num_lost_page++; // 缺页数+1
                int time_max = 0;
                auto erase_temp = dq.begin(); // 即将需要被删除的页面
                // 在序列中往后找，找最远才被访问的那个编号
                std::map<int,int> target_map;
                for (int j = i+1; j < num_array.size(); j++)
                {
                    int diff = j-i ; //该页面距离现在访问的时间
                    // 不在map里面才插入
                    if(target_map.find(num_array[j]) == target_map.end())
                    {
                        target_map.emplace(num_array[j],diff);
                    }
                }
                // 循环结束后，我们会得到一张未来需要访问的页面号，和这些页面号未来第一次访问距离现在的时间
                // 需要做的就是，从当前主存中，查询未来还需要访问的页面号，并找到第一次访问距离现在最长那个的那个页面
                int max_time = 0;
                for (auto pos = dq.begin(); pos != dq.end(); pos++) {
                    // 如果有某一个页面在map里面没有找到，代表它未来不再需要访问，直接剔除
                    auto it = target_map.find(*pos);
                    if(it == target_map.end()){
                        erase_temp = pos;
                        break; // 这个页面就是需要被删除的那个
                    }
                    // 更新max_time，即最长才被访问的页面
                    if(it->second > max_time){
                        max_time = it->second;
                        erase_temp = pos;
                    }
                }
                std::cout << "队列中未来最长没有被使用的页面为：" << *erase_temp << std::endl;
                dq.erase(erase_temp);
                // 插入新的
                dq.push_back(in);
            }
        }
    }
    std::cout << std::endl;
    std::cout << "opt缺页次数为: " << num_lost_page << std::endl;
    std::cout << "opt缺页率为：" << ((num_lost_page * 1.0 / n) * 100) << "%" << std::endl;
}
