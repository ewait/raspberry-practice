#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <unistd.h>

// 消息结构体
struct msg_buffer
{
    long msg_type;
    int data;
};

// 服务器进程
void server_process(int msg_queue_id)
{
    struct msg_buffer msg_rcv, msg_send;

    // 服务器等待接收类型为REQ的消息
    msgrcv(msg_queue_id, &msg_rcv, sizeof(msg_rcv.data), 1, 0);

    // 显示为客户端服务
    printf("Serving for client %d\n", msg_rcv.data);

    // 向客户端发送应答消息
    msg_send.msg_type = msg_rcv.data;
    msg_send.data = getpid();  // 服务器进程的标识数
    msgsnd(msg_queue_id, &msg_send, sizeof(msg_send.data), 0);
}

// 客户端进程
void client_process(int msg_queue_id)
{
    struct msg_buffer msg_send, msg_rcv;

    // 向服务器发送请求消息
    msg_send.msg_type = 1;
    msg_send.data = getpid();  // 客户端进程的标识数
    msgsnd(msg_queue_id, &msg_send, sizeof(msg_send.data), 0);

    // 等待接收服务器的应答消息
    msgrcv(msg_queue_id, &msg_rcv, sizeof(msg_rcv.data), getpid(), 0);

    // 显示接收到的应答
    printf("Receive reply from server %d\n", msg_rcv.data);
}

int main()
{
    key_t key = 75;  // 关键字为75
    int msg_queue_id;

    // 创建消息队列
    msg_queue_id = msgget(key, 0666 | IPC_CREAT);
    if (msg_queue_id == -1)
    {
        perror("msgget");
        exit(EXIT_FAILURE);
    }

    pid_t pid = fork();

    if (pid == -1)
    {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (pid > 0)
    {
        // 父进程作为服务器进程
        server_process(msg_queue_id);
    } else
    {
        // 子进程作为客户端进程
        client_process(msg_queue_id);
    }

    sleep(2);
    // 父子进程都执行完通信后再删除消息队列
    if (pid > 0) {
        if (msgctl(msg_queue_id, IPC_RMID, NULL) == -1) {
            perror("msgctl");
            exit(EXIT_FAILURE);
        }
    }

    return 0;
}
