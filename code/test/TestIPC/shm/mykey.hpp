#pragma once

#include <iostream>
#include <cstring>
#include <cstdlib>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <fcntl.h>
#include <unistd.h>
using namespace std;

#define NUM 1024
#define PROJ_ID 0x20
#define PATH_NAME "/home/lyk/ubt-wsl/test_code_wsl/my_linux_code/code/test/TestIPC/shm"

// 保证二者可以获取到一个相同的pathname和key
key_t CreateKey()
{
    key_t key = ftok(PATH_NAME, PROJ_ID);
    if(key < 0)
    {
        cerr <<"ftok: "<< strerror(errno) << endl;
        exit(1);//key获取错误直接退出程序
    }
    return key;
}

// 通过检查共享内存的第一个字节，确认对方是否就绪
// 共享内存的前四个字节为-1，代表对方就绪
bool ClientWait(char* buf)
{
    char* ptr = buf;
    if((*((int*)ptr)) != -1) // 访问第一个int的位置
    {
        return false;
    }
    return true;
}

// 通过检查共享内存的第一个字节，确认对方是否就绪
// 共享内存的前四个字节为0，代表对方就绪
bool ServerWait(char* buf)
{
    char* ptr = buf;
    if((*((int*)ptr)) == -1) // 访问第一个int的位置
    {
        return false;
    }
    return true;
}