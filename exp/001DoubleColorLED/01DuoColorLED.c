
#include <wiringPi.h>
#include <softPwm.h>
#include <stdio.h>
#include <unistd.h>

#define uchar unsigned char

#define makerobo_Led_PinRed    0 // 红色LED 管脚
#define makerobo_Led_PinGreen  1 // 绿色LED 管脚

// LED 初始化
void makerobo_led_Init(void)
{
	softPwmCreate(makerobo_Led_PinRed,  0, 100);
	softPwmCreate(makerobo_Led_PinGreen,0, 100);
}
// 设置LED 亮度PWM调节范围是0x00-0xff
void makerobo_led_ColorSet(uchar r_val, uchar g_val)
{
	softPwmWrite(makerobo_Led_PinRed,   r_val);
	softPwmWrite(makerobo_Led_PinGreen, g_val);
}

// // 自定义的delay函数，延时z毫秒
// void delay(unsigned int z)
// {
//     unsigned int i,j;
//     for(i=z;i>0;i--)
//         for(j=11000;j>0;j--);  // 利用无实际意义的for循环来进行延时
// }
//linux下使用delay函数需要调用系统头文件 <unistd.h>

int main()
{
    //初始化连接失败时，将消息打印到屏幕
	if(wiringPiSetup() == -1){ 
		printf("setup wiringPi failed !\n");
		return 1; 
	}

	makerobo_led_Init(); // LED 初始化
	//int k=2;
	while(1){
		makerobo_led_ColorSet(0xff,0x00);   //红色	
		delay(5000);                         //延时500ms
		makerobo_led_ColorSet(0x00,0xff);   //绿色
		delay(5000);
		makerobo_led_ColorSet(0xff,0x45);	
		delay(5000);
		makerobo_led_ColorSet(0xff,0xff);	
		delay(5000);
		makerobo_led_ColorSet(0x7c,0xfc);	
		delay(5000);
	}
	return 0;
}
