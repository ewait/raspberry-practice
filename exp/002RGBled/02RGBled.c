#include <wiringPi.h>
#include <softPwm.h>
#include <stdio.h>

#include <unistd.h>//linux下使用delay函数需要该头文件

#define uchar unsigned char//利用无符号整形控制输入数据的范围在0-255

#define makerobo_Led_PinRed    0 // 红色LED 管脚
#define makerobo_Led_PinGreen  1 // 绿色LED 管脚
#define makerobo_Led_PinBlue   2 // 蓝色LED 管脚

// LED 初始化
void makerobo_led_Init()
{//第三个参数是range，将pwm设置成100（全开）
	softPwmCreate(makerobo_Led_PinRed,  0, 100);
	softPwmCreate(makerobo_Led_PinGreen,0, 100);
	softPwmCreate(makerobo_Led_PinBlue, 0, 100);
}
// LED 颜色设置
void makerobo_led_Color_Set(uchar r_val, uchar g_val, uchar b_val)
{//对应不同颜色针脚的设置，如果需要红色，就只给红色r_val传对应值
 //给不同颜色输入不同值，达成混色的效果！
	softPwmWrite(makerobo_Led_PinRed,   r_val);
	softPwmWrite(makerobo_Led_PinGreen, g_val);
	softPwmWrite(makerobo_Led_PinBlue,  b_val);
}


int main()
{
    //初始化连接失败时，将消息打印到屏幕
	if(wiringPiSetup() == -1){
		printf("setup wiringPi failed !");
		return 1; 
	}
	makerobo_led_Init();
	
	//pinMode (LED,OUTPUT);这个是控制单个led的函数-需要把针脚设置为output模式
	//暂时不使用它
	
	int n=0;
	printf("请输入循环周期的次数>");
	scanf("%d",&n);//输入循环周期的次数
	while(n--)
	{
		makerobo_led_Color_Set(0xff,0x00,0x00);   //红色	
		delay(500);   //延时500ms，使更改便于观察
		makerobo_led_Color_Set(0x00,0xff,0x00);   //绿色
		delay(500);                   
		makerobo_led_Color_Set(0x00,0x00,0xff);   //蓝色
		delay(500);

		makerobo_led_Color_Set(0xff,0xff,0x00);   //黄色
		delay(500);                 
		makerobo_led_Color_Set(0xff,0x00,0xff);   //粉色
		delay(500);     
		makerobo_led_Color_Set(0xff,0xff,0xff);   //白色
		delay(500);       

		makerobo_led_Color_Set(0x94,0x00,0xd3);   //紫色
		delay(500);
		makerobo_led_Color_Set(0x76,0xee,0x00);   //偏黄色
		delay(500);
		makerobo_led_Color_Set(0x00,0xc5,0xcd);	  //淡蓝色
		delay(500);
		
		//makerobo_led_Color_Set(0x00,0x00,0x00);	//参数都为0,相当于关灯
		//delay(500);//如果不延时，效果无法展示出来
	}
	
	//最后循环结束时，关闭LED（如果不这么设置，LED灯会停留在最后一个颜色）
	makerobo_led_Color_Set(0x00,0x00,0x00);	//参数都为0,相当于关灯
	delay(500);//如果不延时，效果无法展示出来
	
	return 0;
}
