#include <wiringPi.h>
#include <stdio.h>

#define makerobo_LaserPin 0  //定义激光传感器管脚

int main()
{
	//初始化连接失败时，将消息打印到屏幕
	if(wiringPiSetup() == -1){ 
		printf("setup wiringPi failed !");
		return 1; 
	}
	
	pinMode(makerobo_LaserPin, OUTPUT); // 激光传感器设置为输出模式
	
	int k=2;
	while(k--)
	{
		digitalWrite(makerobo_LaserPin, HIGH); //打开激光传感器
		delay(1000); //延时1s                           
		digitalWrite(makerobo_LaserPin, LOW);  //关闭激光传感器
		delay(1000);//延时1s    
	}
	
	digitalWrite(makerobo_LaserPin, LOW); //循环结束后，恢复关闭状态
	delay(500); 
	
	return 0;
}
