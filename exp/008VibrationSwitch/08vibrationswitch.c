#include <wiringPi.h>
#include <stdio.h>

#define makerobo_VibratePin	0   // 震动传感器
#define makerobo_Rpin		1   // 红色LED PIN 
#define makerobo_Gpin		2   // 绿色LED PIN

int clb_tmp = 0;//全局变量用于比较
//双色LED控制函数
void double_colorLED(int color)
{
	pinMode(makerobo_Rpin, OUTPUT); //设置为输出模式
	pinMode(makerobo_Gpin, OUTPUT); //设置为输出模式

	if (0 == color)             //点亮红色LED灯
	{
		digitalWrite(makerobo_Rpin, HIGH);
		digitalWrite(makerobo_Gpin, LOW);
	}
	else if (1 == color)       //点亮绿色LED灯
	{
		digitalWrite(makerobo_Rpin, LOW);
		digitalWrite(makerobo_Gpin, HIGH);
	}
	else
		printf("Makerobo Double color LED Error\n"); // 双色LED错误
}

// 打印信息,打印出振动传感器的状态	
void makerobo_Print(int x)
{
	if (x != clb_tmp)
	{
		if (x == 0)
			printf("...Makerobo ON\n");
		if (x == 1)
			printf("Makerobo OFF..\n");

		clb_tmp = x;//复位x
	}
}

int main()
{
	int clb_status = 0;  //状态值
	int clb_tmp1 = 0;     //比较值
	int clb_value = 1;   //当前值

	//wiringPi初始化连接失败时，将消息打印到屏幕
	if(wiringPiSetup() == -1){ 
		printf("setup wiringPi failed !");
		return 1; 
	}

    //振动传感器Pin设置为输入模式
	pinMode(makerobo_VibratePin, INPUT);
	
	while(1){
		clb_value = digitalRead(makerobo_VibratePin);  //获取振动传感的值
		if (clb_tmp1 != clb_value)
		{										//振动传感器的输出值发生改变
			clb_status ++;                             //振动传感器状态加1
			if (clb_status > 1)
			{				
				clb_status = 0;     //状态发生改变判断，如果变成2了复位成0
			}
			double_colorLED(clb_status);	          //控制双色LED模块
			makerobo_Print(clb_status);               //打印出状态
			delay(1000);                              //延时1s
		}
	}
	return 0;
}
