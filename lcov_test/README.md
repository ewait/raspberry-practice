这个目录下是和lcov代码测试覆盖率检测有关的信息。

## 依赖

使用之前，请先安装lcov2.0和google gtest

### 安装lcov2.0

```bash
wget https://github.com/linux-test-project/lcov/releases/download/v2.0/lcov-2.0.tar.gz
tar -zxvf lcov-2.0.tar.gz
cd lcov-2.0
sudo make install
```

### 系统依赖项

```bash
# ubuntu安装依赖项
sudo apt install -y perl libcapture-tiny-perl libdatetime-perl
# centos安装依赖项比较麻烦
sudo apt install -y perl
sudo perl -MCPAN -e 'install "包名"'
```

对于centos而言，请参考lcov仓库的readme，用perl命令安装缺少的包。

```
These perl packages include:

  - Capture::Tiny
  - DateTime
  - Devel::Cover
  - Digest::MD5
  - File::Spec
  - at least one flavor of JSON module.
    In order of performance/preference:
       - JSON::XS
       - Cpanel::JSON::XS
       - JSON::PP
       - JSON
 - Memory::Process
 - Module::Load::Conditional
 - Scalar::Util
 - Time::HiRes

If your system is missing any of these, then you may be able to install them
via:

   $ perl -MCPAN -e 'install "packageName"'
```

你可能连不上perl的moudle源，可以百度修改镜像源的办法，或者直接使用清华源

[https://mirrors.tuna.tsinghua.edu.cn/help/CPAN/](https://mirrors.tuna.tsinghua.edu.cn/help/CPAN/)


### goole gtest安装教程

```bash
unzip googletest-1.12.x.zip
cd googletest-1.12.x
cmake CMakeLists.txt
make
sudo make install
```

为了方便，我在本仓库下直接上传了[lcov和gtest的安装包](./package)，你可以直接使用它们。实测centos8和ubuntu22.04都可以使用，安装命令相同。

## 使用

直接 `make lcov` 就行了。会生成一个`coverage_report`路径，内部有html文档。