#include <gtest/gtest.h>
#include "main.hpp"

TEST(SubTest, SubTest1)
{
    // EXPECT_EQ(Sub(3, 2), 1);  // 期望 result 等于 1
    EXPECT_EQ(Sub(2, 4, false), -2);  // 期望 result 等于 2
    EXPECT_EQ(Sub(2, 4, true), 2);
    EXPECT_EQ(Sub(6, 4, true), 2);
    // 期望抛出异常
    EXPECT_ANY_THROW(Sub(100, 400, true));
    // EXPECT_EQ(Sub(3, 3), 0);  // 期望 result 等于 0
}

TEST(SubTest, SubTest2)
{
    EXPECT_NO_THROW(CallSub());
    EXPECT_NO_THROW(QueueTest());
}

TEST(EmplaceMapTest,EmplaceMapTest1)
{
    EXPECT_NO_THROW(EmplaceMap(1,2));
    EXPECT_NO_THROW(EmplaceMap(1,3));
}

// test code
TEST(teststring, teststringplus)
{
    string local = "test1";  
    string remote = "test12";
    EXPECT_NO_THROW(test_string_plus(local, remote));
    local = "test21";
    remote = "test2";  
    EXPECT_NO_THROW(test_string_plus(local, remote));
}

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}