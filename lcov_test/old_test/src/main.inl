#include "main.hpp"


// This is just a lcov test function, it does not have actual functionality
void test_string_plus_inl(const std::string &local, const std::string &remote)
{
    static std::string recv_msg;
    std::string target("/");
    if (local < remote)
    {
        target += local + "_" + remote;
    } else
    {
        target += remote + "_" + local;
    }

    char sun_path[128];
    static_cast<void>(strncpy(sun_path, (recv_msg).c_str(), sizeof(sun_path) - 1));
}
