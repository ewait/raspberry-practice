#include <gtest/gtest.h>

#include "src/main.hpp"

// // 定义一个测试案例
// TEST(AddTest, TestAddition) {
//     // 调用普通函数
//     int result = Add(2, 3);

//     // 使用 GTest 断言宏检查结果是否符合预期
//     EXPECT_EQ(result, 5); // 期望 result 等于 5
// }

TEST(SubTest, TestSub)
{
    EXPECT_EQ(Sub(3, 2), 1);  // 期望 result 等于 5
    // EXPECT_EQ(Sub(10, 3), 7); // 期望 result 等于 5
}

TEST(MapTest, TestMap)
{
    // EXPECT_NO_THROW(test_emplace(1,1, "test1"));
    // EXPECT_NO_THROW(test_emplace(2,2, "test2"));
    // EXPECT_NO_THROW(test_emplace(3,3, "test3"));
    // EXPECT_NO_THROW(test_emplace(2,4, "test4"));
    EXPECT_NO_THROW(test_emplace(1, 5));
    // EXPECT_NO_THROW(test_emplace(2,2));
    // EXPECT_NO_THROW(test_emplace(3,3));
    // EXPECT_NO_THROW(test_emplace(2,4));

    EXPECT_ANY_THROW(test_emplace(2, -4));
    EXPECT_ANY_THROW(test_emplace(3, -50));
}

TEST(VetorTest, TestEmplace)
{
    EXPECT_NO_THROW(test_vector_emplace(1, 10));
    EXPECT_ANY_THROW(test_vector_emplace(2, -4));
    EXPECT_ANY_THROW(test_vector_emplace(3, -50));
}

TEST(testdefault, testdefault)
{
    EXPECT_NO_THROW(test_default_value1(1));
    EXPECT_NO_THROW(test_default_value2());
    is_flag = false;
    EXPECT_NO_THROW(test_default_value1(2));
    EXPECT_NO_THROW(test_default_value2());
}

TEST(testexp, testexp)
{
    EXPECT_NO_THROW(test_exception(false));
    EXPECT_NO_THROW(test_exception(true));
}

template <int n>
class test_exp_class
{
public:
    test_exp_class()
    {
        if (_a <= 0)
        {
            throw std::runtime_error("init failed");
        }
    }

    int _a = n;
};

TEST(testexp, testnew)
{
    test_new(1);
    test_new(10);
    EXPECT_NO_THROW(test_new(10));
    EXPECT_NO_THROW(test_new_array(10));
    // (test_new_array(100000000000000000));

    EXPECT_NO_THROW(int *t = SafeMallocObject<int>());
    EXPECT_NO_THROW(double *t = SafeMallocObject<double>());
    EXPECT_NO_THROW(string *t = SafeMallocObject<string>());
    EXPECT_NO_THROW(test_exp_class<1> *c1 = SafeMallocObject<test_exp_class<1>>());
    EXPECT_ANY_THROW(test_exp_class<-1> *c2 = SafeMallocObject<test_exp_class<-1>>());
}

TEST(testmutex, testlock)
{
    // gmt.lock();
    // EXPECT_EQ(test_mutex_lock(), false);
    // gmt.unlock();
    EXPECT_EQ(test_mutex_lock(), true);
    // lock guard
    EXPECT_NO_THROW(test_mutex_lockguard());
}

TEST(teststring, testassign)
{
    EXPECT_NO_THROW(test_string_assign());
}

TEST(teststring, teststringassign)
{
    char recv_buf[1024];
    strcpy(recv_buf, "test5");
    uint64_t recv_bytes = 5;
    EXPECT_NO_THROW(test_string_assign(recv_buf, recv_bytes));
    recv_bytes = 0;
    EXPECT_NO_THROW(test_string_assign(recv_buf, recv_bytes));
    // 尝试使用max size让string抛出长度异常
    string temp;
    recv_bytes = temp.max_size();
    printf("max size: %lu\n", recv_bytes);
    EXPECT_ANY_THROW(test_string_assign(recv_buf, recv_bytes));  // 测试发现会抛出bad alloc错误，无法申请内存
}

TEST(teststring, teststringplus)
{
    string local = "test1";  // 小的那个
    string remote = "test12";
    EXPECT_NO_THROW(test_string_plus(local, remote));
    local = "test21";
    remote = "test2";  // 小的那个
    EXPECT_NO_THROW(test_string_plus(local, remote));

}

TEST(testfor,testautofor)
{
    _test_map_v.insert({1,{1,2,3,4}});
    _test_map_v.insert({2,{2,2,3,4}});
    _test_map_v.insert({3,{3,2,3,4}});

    EXPECT_NO_THROW(test_auto_for(1));
    EXPECT_ANY_THROW(test_auto_for(5));
}


void test1()
{
    test_string_plus("test", "1");
    test_string_plus("2", "test");
}

void test2()
{
    test_string_plus_inl("test", "3");
    test_string_plus_inl("4", "test");
}

TEST(teststring,teststringplus_inl)
{
    test1();
    test2();
}

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}