## 介绍

这两个是树莓派**开机自动连接wifi**的配置文件

### 使用方法

使用方法如下：

~~~
network={
	ssid="zhulin"
	psk="12345678"
	priority=5
}
~~~

* ssid填wifi名字
* psk填wifi密码
* priority是连接的优先级，数字越大越高

你可以在后续追加`network`，保存多个wifi信息，填写方法相同

----

编写完后，请将`ssh`文件和`wpa_supplicant.conf`都放入树莓派的启动盘，再开机树莓派。理论上这样就能自动连接到你的wifi了。

在wifi管理后台里面找到树莓派，查看ip，即可使用ssh或者vnc远程连接上了。

> 注意：这个配置文件和ssh文件会在树莓派**开机后消失不见**，如果需要修改wifi配置，重新编写新的`wpa_supplicant.conf`文件丢入树莓派的启动盘即可。